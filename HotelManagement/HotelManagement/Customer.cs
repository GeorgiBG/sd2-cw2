﻿using System.Collections.Generic;

namespace HotelManagement
{
    public class Customer : Person
    {
        private int customerRef;
        private string address;

        private DBSingleton Database = DBSingleton.Instance();

        #region GETTERS/SETTERS
        public int CustomerRef
        {
            get
            {
                return customerRef;
            }
            set
            {
                customerRef = value;
            }
        }

        public string Address
        {
            get
            {
                return address;
            }
            protected set
            {
                address = value;
            }
        }
        #endregion

        public Customer() : base() { }

        public Customer(string firstName, string lastName, string address) : base(firstName, lastName)
        {
            if (Database.GetLastCustomerReferenceNumber() != 0)
                CustomerRef = Database.GetLastCustomerReferenceNumber() + 1;
            Address = address;
        }

        public void AddNewCustomer()
        {
            Dictionary<string, string> customerDetails = new Dictionary<string, string>();
            // Set details content
            customerDetails.Add("CustomerReferenceNumber", (Database.GetLastCustomerReferenceNumber() > 0 ? (Database.GetLastCustomerReferenceNumber() + 1).ToString() : 1.ToString()));
            customerDetails.Add("FirstName", FirstName);
            customerDetails.Add("LastName", LastName);
            customerDetails.Add("Address", Address);
            // Add to DB
            Database.SetCustomer(customerDetails);
        }
    }
}
