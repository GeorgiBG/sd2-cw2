﻿using System.Collections.Generic;
using System.Windows;

namespace HotelManagement
{
    public partial class CustomerDetailsWindow
    {
        private DBSingleton Database = DBSingleton.Instance();

        int customerReferenceNum;

        string firstName;
        string lastName;
        string address;

        public CustomerDetailsWindow(int customerRef)
        {
            InitializeComponent();

            customerReferenceNum = customerRef;
            customerName.Content = "Edit customer details";

            ShowCustomerDetails();
        }

        private void ShowCustomerDetails()
        {
            firstName = Database.GetCustomerByReferenceNum(customerReferenceNum).FirstName;
            lastName = Database.GetCustomerByReferenceNum(customerReferenceNum).LastName;
            address = Database.GetCustomerByReferenceNum(customerReferenceNum).Address;
            
            customerDetailsBox.Content = "Customer: " + firstName + " " + lastName;

            editCustFirstNameTxtBox.Text = firstName;
            editCustLastNameTxtBox.Text = lastName;
            editCustAddressTxtBox.Text = address;
        }

        private void editCustomerSaveBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Dictionary<string, string> customerDetails = new Dictionary<string, string>();

            firstName = editCustFirstNameTxtBox.Text;
            lastName = editCustLastNameTxtBox.Text;
            address = editCustAddressTxtBox.Text;

            customerDetails.Add("FirstName", firstName);
            customerDetails.Add("LastName", lastName);
            customerDetails.Add("Address", address);

            Database.UpdateCustomerByReferenceNum(customerReferenceNum, customerDetails);

            MessageBox.Show("Edited!");
            this.Close();
        }

        private void editCustomerDeleteBtn_Click(object sender, RoutedEventArgs e)
        {
            Database.DeleteCustomer(customerReferenceNum);
            this.Close();
        }
    }
}
