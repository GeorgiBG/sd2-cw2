﻿using System.Collections.Generic;

namespace HotelManagement
{
    class NutFree : ExtraDecorator
    {
        private DBSingleton Database = DBSingleton.Instance();

        public NutFree(ExtraItem extra) : base(extra)
        {
            Specification = "Nuts Free";
            if (!Description.Contains(Specification))
                Description += " (" + Specification + ")";
        }

        public override void AddNewExtra()
        {
            Dictionary<string, string> extraDetails = new Dictionary<string, string>();

            extraDetails.Add("ExtraId", (Database.GetLastExtraId() + 1).ToString());
            extraDetails.Add("CarHireId", 0.ToString());
            extraDetails.Add("Description", Description);
            extraDetails.Add("Cost", Cost.ToString());

            // Add to DB
            Database.SetExtra(extraDetails);
        }
    }
}
