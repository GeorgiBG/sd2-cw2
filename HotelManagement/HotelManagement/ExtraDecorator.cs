﻿namespace HotelManagement
{
    // DECORATOR DESIGN PATTERN
    abstract class ExtraDecorator : ExtraItem
    {
        private string specification;
        protected ExtraItem extraItem;

        private DBSingleton Database = DBSingleton.Instance();

        public string Description
        {
            get
            {
                return extraItem.Description;
            }
            set
            {
                extraItem.Description = value;
            }
        }

        public decimal Cost
        {
            get
            {
                return extraItem.Cost;
            }
            set
            {
                extraItem.Cost = value;
            }
        }

        public string Specification
        {
            get
            {
                return specification;
            }
            set
            {
                specification = value;
            }
        }


        public ExtraDecorator(ExtraItem extra)
        {
            extraItem = extra;
        }

        public virtual void AddNewExtra()
        {
            // Left for implementation in the decorations
        }
    }
}