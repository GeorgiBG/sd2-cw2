﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace HotelManagement
{
    class Breakfast : ExtraItem
    {
        private string description;
        private decimal cost;

        private DBSingleton Database = DBSingleton.Instance();

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public decimal Cost
        {
            get
            {
                return cost;
            }
            set
            {
                cost = value;
            }
        }

        public Breakfast()
        {
            Description = "Breakfast meal";
            Cost = 5;
        }

        public void AddNewExtra()
        {
            Dictionary<string, string> extraDetails = new Dictionary<string, string>();

            extraDetails.Add("ExtraId", (Database.GetLastExtraId() > 0 ? (Database.GetLastExtraId() + 1).ToString() : 1.ToString()));
            extraDetails.Add("CarHireId", 0.ToString());
            extraDetails.Add("Description", Description);
            extraDetails.Add("Cost", Cost.ToString());

            // Add to DB
            Database.SetExtra(extraDetails);
        }
    }
}