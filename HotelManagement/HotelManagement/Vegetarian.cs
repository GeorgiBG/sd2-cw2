﻿using System.Collections.Generic;

namespace HotelManagement
{
    class Vegetarian : ExtraDecorator
    {
        private DBSingleton Database = DBSingleton.Instance();

        public Vegetarian(ExtraItem extra) : base(extra)
        {
            Specification = "Vegetarian";
            if(!Description.Contains(Specification))
                Description += " (" + Specification + ")";
        }

        public override void AddNewExtra()
        {
            Dictionary<string, string> extraDetails = new Dictionary<string, string>();

            extraDetails.Add("ExtraId", (Database.GetLastExtraId() + 1).ToString());
            extraDetails.Add("CarHireId", 0.ToString());
            extraDetails.Add("Description", Description);
            extraDetails.Add("Cost", Cost.ToString());

            // Add to DB
            Database.SetExtra(extraDetails);
        }
    }
}