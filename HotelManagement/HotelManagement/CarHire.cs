﻿using System;
using System.Collections.Generic;

namespace HotelManagement
{
    class CarHire : ExtraItem
    {
        private string description;
        private decimal cost;
        private string driverName;
        private DateTime hireStartDate;
        private DateTime hireEndDate;

        private DBSingleton Database = DBSingleton.Instance();

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
            }
        }

        public decimal Cost
        {
            get
            {
                return cost;
            }
            set
            {
                cost = value;
            }
        }

        public string DriverName
        {
            get
            {
                return driverName;
            }
            set
            {
                driverName = value;
            }
        }

        public DateTime HireStartDate
        {
            get
            {
                return hireStartDate;
            }
            set
            {
                hireStartDate = value;
            }
        }

        public DateTime HireEndDate
        {
            get
            {
                return hireEndDate;
            }
            set
            {
                hireEndDate = value;
            }
        }

        public CarHire()
        {
            Description = "Car Hiring";
        }

        public void ScheduleDates(DateTime hireStart, DateTime hireEnd)
        {
            HireStartDate = hireStart;
            HireEndDate = hireEnd;
        }

        public void CalculateCost()
        {
            // calculate hire cost
            Cost = decimal.Parse((HireEndDate - HireStartDate).TotalDays.ToString()) * 50;
        }

        public void AddNewExtra()
        {
            Dictionary<string, string> extraDetails = new Dictionary<string, string>();
            Dictionary<string, string> carHireDetails = new Dictionary<string, string>();

            carHireDetails.Add("Id", (Database.GetLastCarHireId() != 0 ? (Database.GetLastCarHireId() + 1).ToString() : 1.ToString()));
            carHireDetails.Add("DriverName", DriverName);
            carHireDetails.Add("HireStartDate", HireStartDate.ToShortDateString());
            carHireDetails.Add("HireEndDate", HireEndDate.ToShortDateString());

            // Add to DB
            Database.SetCarHire(carHireDetails);

            // -------------------------- //

            extraDetails.Add("ExtraId", (Database.GetLastExtraId() > 0 ? (Database.GetLastExtraId() + 1).ToString() : 1.ToString()));
            extraDetails.Add("CarHireId", Database.GetLastCarHireId().ToString());
            extraDetails.Add("Description", Description);
            extraDetails.Add("Cost", Cost.ToString());
            
            // Add to DB
            Database.SetExtra(extraDetails);
        }
    }
}