﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Linq;

namespace HotelManagement
{
    /** [Serializable] -> used to be when did FILE SERIALIZATION **/
    sealed class DBSingleton
    {
        private static readonly DBSingleton _instance = new DBSingleton();

        public HolidayDBDataContext DB { get; private set; }

        // GET/SET the connection string to DB
        string execRunningDir = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
        string connStr = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=F:\BITBUCKET\SD2_Coursework2\cw2-2017\HotelManagement\HotelManagement\NapierHolidays.mdf;Integrated Security=True";
        
        private DBSingleton()
        {
            /* TRY THIS
            string Path = Environment.CurrentDirectory;
            string[] appPath = Path.Split(new string[] { "bin" }, StringSplitOptions.None);
            AppDomain.CurrentDomain.SetData("DataDirectory", appPath[0]);
            */
        }

        public static DBSingleton Instance()
        {
           return _instance;
        }

        /* ################################################ */
        /* ########### DATABASE DATA MANAGEMENT ########### */
        /* ################################################ */
        /* # https://www.linqpad.net/WhyLINQBeatsSQL.aspx # */

        #region CUSTOMER
        public List<NH_Customer> GetAllCustomers()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                List<NH_Customer> data = new List<NH_Customer>();

                var customers = from c in DB.NH_Customers select c;

                foreach (var customer in customers)
                {
                    data.Add(customer);
                }

                return data;
            }
        }

        public List<NH_Customer> GetAllCustomersByName(string name)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                List<NH_Customer> data = new List<NH_Customer>();

                var customers = from c in DB.NH_Customers where
                                c.FirstName.Contains(name)
                                || c.LastName.Contains(name)
                                || (c.FirstName + " " + c.LastName).Contains(name)
                                select c;

                foreach (var customer in customers)
                {
                    data.Add(customer);
                }

                return data;
            }
        }

        public List<NH_Customer> GetAllCustomersByAddress(string address)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                List<NH_Customer> data = new List<NH_Customer>();

                var customers = from c in DB.NH_Customers where c.Address.Contains(address) select c;

                foreach (var customer in customers)
                {
                    data.Add(customer);
                }

                return data;
            }
        }

        public List<NH_Booking> GetCustomerBookings(int customerRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                List<NH_Booking> data = new List<NH_Booking>();

                var bookings = from book
                               in DB.NH_Bookings
                               where book.CustomerRefNum.Equals(customerRef)
                               select book;

                foreach (var booking in bookings)
                {
                    data.Add(booking);
                }

                return data;
            }
        }

        public int GetLastCustomerReferenceNumber()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var lastRefNum = from referenceNum in DB.NH_Customers
                           group referenceNum by true into refNum
                           select new
                           {
                               max = refNum.Max(x => x.ReferenceNum)
                           };
                
                foreach (var lastRef in lastRefNum)
                {
                    return lastRef.max;
                }

                return 0;
            }
        }

        public NH_Customer GetCustomerByReferenceNum(int referenceNum)
        {
            NH_Customer customer = new NH_Customer();

            using (DB = new HolidayDBDataContext(connStr))
            {
                if (DB.NH_Customers.Any(i => i.ReferenceNum.Equals(referenceNum)))
                    customer = DB.NH_Customers.Single(i => i.ReferenceNum.Equals(referenceNum));
                else
                    return null;
            }

            return customer;
        }

        public void SetCustomer(Dictionary<string, string> customerDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_Customer cust = new NH_Customer
                {
                    ReferenceNum = int.Parse(customerDetails["CustomerReferenceNumber"]),
                    FirstName = customerDetails["FirstName"],
                    LastName = customerDetails["LastName"],
                    Address = customerDetails["Address"]
                };

                // Add the new object to the Orders collection.
                DB.NH_Customers.InsertOnSubmit(cust);

                // Submit the change to the database.
                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }

        public void UpdateCustomerByReferenceNum(int referenceNum, Dictionary<string, string> customerDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var customer = (from cust in DB.NH_Customers
                                where cust.ReferenceNum.Equals(referenceNum)
                                select cust).Single();

                customer.FirstName = customerDetails["FirstName"];
                customer.LastName = customerDetails["LastName"];
                customer.Address = customerDetails["Address"];

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }
        #endregion

        #region GUEST
        public List<NH_Guest> GetAllGuestsByBookingRef(int bookingRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                List<NH_Guest> data = new List<NH_Guest>();

                var guests = from guest in DB.NH_Guests
                             join book_guest in DB.NH_BookingGuests 
                             on guest.PassportNum equals book_guest.GuestPassNum
                             where book_guest.BookingRefNum == bookingRef
                             select guest;

                foreach (var guest in guests)
                {
                    data.Add(guest);
                }

                return data;
            }
        }

        public NH_Guest GetGuestByPassportNum(string passportNum)
        {
            NH_Guest guest = new NH_Guest();

            using (DB = new HolidayDBDataContext(connStr))
            {
                guest = DB.NH_Guests.Single(i => i.PassportNum.Equals(passportNum));
            }

            return guest;
        }

        public void SetGuest(Dictionary<string, string> guestDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_Guest guest = new NH_Guest
                {
                    PassportNum = guestDetails["GuestPassportNumber"],
                    FirstName = guestDetails["FirstName"],
                    LastName = guestDetails["LastName"],
                    Age = byte.Parse(guestDetails["Age"])
                };

                DB.NH_Guests.InsertOnSubmit(guest);

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }
        #endregion

        #region EXTRA
        public List<NH_Extra> GetAllExtrasByBookingRef(int bookingRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                List<NH_Extra> data = new List<NH_Extra>();

                var extras = from extra in DB.NH_Extras
                             join book_extra in DB.NH_BookingExtras
                             on extra.ExtraId equals book_extra.ExtraId
                             where book_extra.BookingRefNum == bookingRef
                             select extra;

                foreach (var extra in extras)
                {
                    data.Add(extra);
                }

                return data;
            }
        }

        public int GetLastExtraId()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var lastExtraId = from extraId in DB.NH_Extras
                                 group extraId by true into xId
                                 select new
                                 {
                                     max = xId.Max(x => x.ExtraId)
                                 };

                foreach (var lastId in lastExtraId)
                {
                    return lastId.max;
                }

                return 0;
            }
        }

        public NH_Extra GetExtraById(int extraId)
        {
            NH_Extra extra = new NH_Extra();

            using (DB = new HolidayDBDataContext(connStr))
            {
                extra = DB.NH_Extras.Single(i => i.ExtraId == extraId);
            }

            return extra;
        }

        public void SetExtra(Dictionary<string, string> extraDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                int? carId;
                if (extraDetails.ContainsKey("CarHireId") && extraDetails["CarHireId"] != string.Empty)
                {
                    carId = (extraDetails["CarHireId"] != null) ? int.Parse(extraDetails["CarHireId"]) : 0;
                    if (carId == 0) carId = null;
                }
                else carId = null;

                NH_Extra extra = new NH_Extra
                {
                    ExtraId = int.Parse(extraDetails["ExtraId"]),
                    CarHireId = carId,
                    Description = extraDetails["Description"],
                    Cost = decimal.Parse(extraDetails["Cost"])
                };

                DB.NH_Extras.InsertOnSubmit(extra);

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }

        public Tuple<DateTime, DateTime> GetBookingDates(int bookingRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();

                return new Tuple<DateTime, DateTime>(booking.ArrivalDate, booking.DepartureDate);
            }
        }

        public string GetBookingNotes(int bookingRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();

                return booking.Notes;
            }
        }
        #endregion

        #region CARHIRE
        public int GetLastCarHireId()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var lastCarHireId = from carHireId in DB.NH_CarHires
                                  group carHireId by true into carId
                                  select new
                                  {
                                      max = carId.Max(x => x.Id)
                                  };

                foreach (var lastId in lastCarHireId)
                {
                    return lastId.max;
                }

                return 0;
            }
        }

        public NH_CarHire GetCarHireById(int carHireId)
        {
            NH_CarHire carHire = new NH_CarHire();

            using (DB = new HolidayDBDataContext(connStr))
            {
                carHire = DB.NH_CarHires.Single(i => i.Id == carHireId);
            }

            return carHire;
        }

        public void SetCarHire(Dictionary<string, string> carHireDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_CarHire carHire = new NH_CarHire
                {
                    Id = int.Parse(carHireDetails["Id"]),
                    Driver = carHireDetails["DriverName"],
                    StartDate = DateTime.Parse(carHireDetails["HireStartDate"]),
                    EndDate = DateTime.Parse(carHireDetails["HireEndDate"])
                };

                DB.NH_CarHires.InsertOnSubmit(carHire);

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }
        #endregion

        #region BOOKING
        public int GetLastBookingReferenceNumber()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var lastRefNum = from referenceNum in DB.NH_Bookings
                                 group referenceNum by true into refNum
                                 select new
                                 {
                                     max = refNum.Max(x => x.ReferenceNum)
                                 };

                foreach (var lastRef in lastRefNum)
                {
                    return lastRef.max;
                }

                return 0;
            }
        }

        public void SetBooking(Dictionary<string, string> bookingDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_Booking booking = new NH_Booking
                {
                    ReferenceNum = int.Parse(bookingDetails["ReferenceNumber"]),
                    CustomerRefNum = int.Parse(bookingDetails["CustomerReferenceNumber"]),
                    NumOfGuests = int.Parse(bookingDetails["NumberOfGuests"]),
                    ArrivalDate = DateTime.Parse(bookingDetails["ArrivalDate"]),
                    DepartureDate = DateTime.Parse(bookingDetails["DepartureDate"]),
                    Notes = bookingDetails["CustomerNotes"]
                };

                DB.NH_Bookings.InsertOnSubmit(booking);

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }

        public void SetBookingPayment(int bookingRef, decimal payment)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_Booking booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();

                booking.Payment = payment;

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }

        public void SetBookingExtrasAmount(Dictionary<string, string> bookingDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                int refereNum = int.Parse(bookingDetails["BookingReferenceNumber"]);
                NH_Booking booking = DB.NH_Bookings.Single(book => book.ReferenceNum.Equals(refereNum));
                if (booking != null)
                {
                    booking.Breakfasts = byte.Parse(bookingDetails["Breakfasts"]);
                    booking.Dinners = byte.Parse(bookingDetails["Dinners"]);
                    booking.CarHires = byte.Parse(bookingDetails["CarHires"]);
                }
                else
                {
                    MessageBox.Show("Error in indexing the booking.");
                }

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }

        public void UpdateBooking(Dictionary<string, string> bookingDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                int bookingRef = int.Parse(bookingDetails["BookingRef"]);

                bool bookingExists = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Any();
                if (bookingExists)
                {
                    var booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();

                    booking.ArrivalDate = DateTime.Parse(bookingDetails["ArrivalDate"]);
                    booking.DepartureDate = DateTime.Parse(bookingDetails["DepartureDate"]);
                    booking.NumOfGuests = int.Parse(bookingDetails["NumOfGuests"]);
                    if (bookingDetails["CustomerNotes"] != null && bookingDetails["CustomerNotes"] != string.Empty)
                    booking.Notes = bookingDetails["CustomerNotes"];
                    if (bookingDetails.ContainsKey("Payment") && bookingDetails["Payment"] != null)
                    booking.Payment = decimal.Parse(bookingDetails["Payment"]);
                }
                else
                {
                    MessageBox.Show("Error retrieving data for such booking.");
                }

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }

        public void DeleteBooking(int bookingRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_Booking booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();

                if(booking != null)
                {
                    var bookingExtras = from bookExtra in DB.NH_BookingExtras where bookExtra.BookingRefNum.Equals(booking.ReferenceNum) select bookExtra;
                    var bookingGuests = from bookGuest in DB.NH_BookingGuests where bookGuest.BookingRefNum.Equals(booking.ReferenceNum) select bookGuest;
                    
                    if(bookingExtras.Any())
                    {
                        foreach (var bExtra in bookingExtras)
                        {
                            NH_Extra extra = (from ex in DB.NH_Extras where ex.ExtraId.Equals(bExtra.ExtraId) select ex).Single();
                            var carHires = from ch in DB.NH_CarHires where ch.Id.Equals(extra.CarHireId) select ch;

                            if (carHires.Any())
                                DB.NH_CarHires.DeleteAllOnSubmit(carHires);

                            if (extra != null)
                            {
                                DB.NH_Extras.DeleteOnSubmit(extra);
                            }
                        }

                        DB.NH_BookingExtras.DeleteAllOnSubmit(bookingExtras);
                    }

                    if (bookingGuests.Any())
                    {
                        foreach (var bGuest in bookingGuests)
                        {
                            NH_Guest guest = (from gt in DB.NH_Guests where gt.PassportNum.Equals(bGuest.GuestPassNum) select gt).Single();
                            if(guest != null)
                            {
                                DB.NH_Guests.DeleteOnSubmit(guest);
                            }
                        }

                        DB.NH_BookingGuests.DeleteAllOnSubmit(bookingGuests);
                    }

                    DB.NH_Bookings.DeleteOnSubmit(booking);

                    try
                    {
                        DB.SubmitChanges();
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show("ERROR: " + e.ToString());
                    }
                }
            }
        }

        public Dictionary<string, string> GetBookingInvoice(int bookingRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_Booking booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();

                Dictionary<string, string> bookingInvoiceDetails = new Dictionary<string, string>();

                bookingInvoiceDetails.Add("TotalGuests", booking.NumOfGuests.ToString());
                bookingInvoiceDetails.Add("ArrivalDate", booking.ArrivalDate.ToString());
                bookingInvoiceDetails.Add("DepartureDate", booking.DepartureDate.ToString());

                if (booking.Payment != null)
                    bookingInvoiceDetails.Add("TotalPayment", booking.Payment.ToString());
                else
                    bookingInvoiceDetails.Add("TotalPayment", "0.00");

                var bookingExtras = DB.NH_BookingExtras.Where(bookExtra => bookExtra.BookingRefNum.Equals(bookingRef));

                int brCount = 0, dinCount = 0;
                double hireDates = 0;

                // ADD INDIVIDUALS
                foreach (var bookingExtra in bookingExtras)
                {
                    var breakfastExtras = DB.NH_Extras.Where(bookBreak => bookBreak.ExtraId.Equals(bookingExtra.ExtraId)
                                                                          && bookBreak.Description.Contains("Breakfast"));

                    var dinnerExtras = DB.NH_Extras.Where(bookDin => bookDin.ExtraId.Equals(bookingExtra.ExtraId)
                                                                          && bookDin.Description.Contains("Dinner"));

                    var carHireExtras = DB.NH_Extras.Where(bookCar => bookCar.ExtraId.Equals(bookingExtra.ExtraId)
                                                                          && bookCar.Description.Contains("Car"));
                    
                    if (breakfastExtras.Any())
                    {
                        foreach (var breakfasts in breakfastExtras)
                        {
                            brCount++;
                            bookingInvoiceDetails.Add("Breakfast" + brCount, breakfasts.Description + ": £5/each");
                        }
                    }

                    if (dinnerExtras.Any())
                    {
                        foreach (var dinners in dinnerExtras)
                        {
                            dinCount++;
                            bookingInvoiceDetails.Add("Dinner" + dinCount, dinners.Description + ": £15/each");
                        }
                    }

                    if (carHireExtras.Any())
                    {
                        foreach (var carExtra in carHireExtras)
                        {
                            NH_CarHire carHire = DB.NH_CarHires.Single(car => car.Id.Equals(carExtra.CarHireId));

                            hireDates = (carHire.EndDate - carHire.StartDate).TotalDays;
                            bookingInvoiceDetails.Add("CarHire", carExtra.Description + ": £50/day" 
                                + " (" + carHire.StartDate.ToShortDateString() + " - " + carHire.EndDate.ToShortDateString() + ")");
                        }
                    }
                }

                // ADD TOTALS
                if (booking.Breakfasts != null)
                    bookingInvoiceDetails.Add("BreakfastTotal", (decimal.Parse(booking.Breakfasts.ToString()) * 5).ToString() + " (x" + booking.Breakfasts + " breakfasts)");
                else
                    bookingInvoiceDetails.Add("BreakfastTotal", "0.00");

                if (booking.Dinners != null)
                    bookingInvoiceDetails.Add("DinnerTotal", (decimal.Parse(booking.Dinners.ToString()) * 15).ToString() + " (x" + booking.Dinners + " dinners)");
                else
                    bookingInvoiceDetails.Add("DinnerTotal", "0.00");

                if (booking.CarHires != null && hireDates > 0)
                    bookingInvoiceDetails.Add("CarHireTotal", (decimal.Parse(booking.CarHires.ToString()) * 50 * decimal.Parse(hireDates.ToString())).ToString() + " (x" + ((hireDates == 1) ? hireDates.ToString() + " day)" : hireDates.ToString() + " days)"));
                else
                    bookingInvoiceDetails.Add("CarHireTotal", "0.00");

                return bookingInvoiceDetails;
            }
        }

        public NH_Customer GetCustomerByBookingReference(int bookingRef)
        {
            NH_Customer customer = new NH_Customer();

            using (DB = new HolidayDBDataContext(connStr))
            {
                customer = (from cust in DB.NH_Customers
                                join book in DB.NH_Bookings on cust.ReferenceNum equals book.CustomerRefNum
                                where book.ReferenceNum.Equals(bookingRef) select cust).Single();
            }

            return customer;
        }

        public void DeleteCustomer(int customerRef)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var customer = (from cust in DB.NH_Customers where cust.ReferenceNum.Equals(customerRef) select cust).Single();
                var bookings = (from book in DB.NH_Bookings where book.CustomerRefNum.Equals(customer.ReferenceNum) select book);
                if (customer != null)
                {
                    if(!bookings.Any())
                    {
                        DB.NH_Customers.DeleteOnSubmit(customer);

                        try
                        {
                            DB.SubmitChanges();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("ERROR: " + e.ToString());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Cannot Delete.");
                    }
                }
                else
                {
                    MessageBox.Show("No such customer.");
                }
            }
        }

        public void DeleteBookingGuest(int bookingRef, string guestPassNum)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();
                if (booking != null)
                {
                    var guest = (from gt in DB.NH_Guests where gt.PassportNum.Equals(guestPassNum) select gt).Single();
                    if (guest != null)
                    {
                        var bookingGuest = (from bookGuest in DB.NH_BookingGuests
                                            join gt in DB.NH_Guests on bookGuest.GuestPassNum equals gt.PassportNum
                                            where (bookGuest.BookingRefNum.Equals(booking.ReferenceNum) && (gt.PassportNum.Equals(guest.PassportNum)))
                                            select bookGuest).Single();

                        DB.NH_Guests.DeleteOnSubmit(guest);
                        DB.NH_BookingGuests.DeleteOnSubmit(bookingGuest);

                        try
                        {
                            DB.SubmitChanges();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("ERROR: " + e.ToString());
                        }
                    }
                }
            }       
        }

        public void DeleteBookingExtra(int bookingRef, int extraId)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var booking = (from book in DB.NH_Bookings where book.ReferenceNum.Equals(bookingRef) select book).Single();
                if (booking != null)
                {
                    var extra = (from ex in DB.NH_Extras where ex.ExtraId.Equals(extraId) select ex).Single();
                    if (extra != null)
                    {
                        var bookingExtra = (from bookExtra in DB.NH_BookingExtras
                                            join ex in DB.NH_Extras on bookExtra.ExtraId equals ex.ExtraId
                                            where (bookExtra.BookingRefNum.Equals(booking.ReferenceNum) && (ex.ExtraId.Equals(extra.ExtraId)))
                                            select bookExtra).Single();

                        DB.NH_Extras.DeleteOnSubmit(extra);
                        DB.NH_BookingExtras.DeleteOnSubmit(bookingExtra);

                        try
                        {
                            DB.SubmitChanges();
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show("ERROR: " + e.ToString());
                        }
                    }
                }
            }
        }
        #endregion

        #region BOOKING_GUEST
        public int GetLastBookingGuestId()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var lastId = from id in DB.NH_BookingGuests
                                 group id by true into lid
                                 select new
                                 {
                                     max = lid.Max(x => x.Id)
                                 };
                
                foreach (var last in lastId)
                {
                    return last.max;
                }

                return 0;
            }
        }

        public void SetBookingGuest(Dictionary<string, string> bookingGuestDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_BookingGuest bookingGuest = new NH_BookingGuest
                {
                    Id = int.Parse(bookingGuestDetails["Id"]),
                    BookingRefNum = int.Parse(bookingGuestDetails["BookingReferenceNumber"]),
                    GuestPassNum = bookingGuestDetails["GuestPassportNumber"]
                };

                DB.NH_BookingGuests.InsertOnSubmit(bookingGuest);

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }
        #endregion

        #region BOOKING_EXTRA
        public int GetLastBookingExtraId()
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                var lastId = from id in DB.NH_BookingExtras
                             group id by true into lid
                             select new
                             {
                                 max = lid.Max(x => x.Id)
                             };

                foreach (var last in lastId)
                {
                    return last.max;
                }

                return 0;
            }
        }

        public void SetBookingExtra(Dictionary<string, string> bookingExtraDetails)
        {
            using (DB = new HolidayDBDataContext(connStr))
            {
                NH_BookingExtra bookingExtra = new NH_BookingExtra
                {
                    Id = int.Parse(bookingExtraDetails["Id"]),
                    BookingRefNum = int.Parse(bookingExtraDetails["BookingReferenceNumber"]),
                    ExtraId = int.Parse(bookingExtraDetails["ExtraId"]),
                };

                DB.NH_BookingExtras.InsertOnSubmit(bookingExtra);

                try
                {
                    DB.SubmitChanges();
                }
                catch (Exception e)
                {
                    MessageBox.Show("ERROR: " + e.ToString());
                }
            }
        }
        #endregion
    }
}
