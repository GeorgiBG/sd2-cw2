﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace HotelManagement
{
    public partial class CustomerBookingsView
    {
        private DBSingleton Database = DBSingleton.Instance();

        int customerReferenceNum;

        int bookingRefToPass;

        public CustomerBookingsView(int customerRef)
        {
            InitializeComponent();

            customerReferenceNum = customerRef;

            ShowCustomerBookings();
        }

        private void bookEdit_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BookingsDataGrid.SelectedItem == null)
            {
                MessageBox.Show("No booking was selected.");
                return;
            }

            BookingDetailsWindow bookingDetailsWindow = new BookingDetailsWindow(bookingRefToPass);
            bookingDetailsWindow.ShowDialog();
        }

        private void bookDelete_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (BookingsDataGrid.SelectedItem == null)
            {
                MessageBox.Show("No booking was selected.");
                return;
            }

            Database.DeleteBooking(bookingRefToPass);
        }

        // Helper methods
        private void ShowCustomerBookings()
        {
            List<NH_Booking> allBookings = Database.GetCustomerBookings(customerReferenceNum);

            HashSet<BookingItem> bookingItems = new HashSet<BookingItem>();
            foreach (var book in allBookings)
            {
                BookingItem bookItem = new BookingItem(book.ReferenceNum, book.ArrivalDate, book.DepartureDate, book.NumOfGuests);

                if (!bookingItems.Contains(bookItem))
                    bookingItems.Add(bookItem);
            }

            BookingsDataGrid.ItemsSource = bookingItems;
        }

        private void BookingsDataGrid_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (BookingsDataGrid.SelectedItem == null) return;

            BookingItem bookRow = BookingsDataGrid.SelectedItem as BookingItem;
            bookingRefToPass = bookRow.Ref;
        }

        private void bookRefresh_Click(object sender, RoutedEventArgs e)
        {
            ShowCustomerBookings();
        }

        private void bookInvoice_Click(object sender, RoutedEventArgs e)
        {
            if(BookingsDataGrid.SelectedItem == null)
            {
                MessageBox.Show("No booking was selected.");
                return;
            }

            var invoiceDetails = Database.GetBookingInvoice(bookingRefToPass);
            BookingInvoice bookingInvoice = new BookingInvoice(bookingRefToPass);
            bookingInvoice.ShowDialog();
        }
    }

    class BookingItem
    {
        public int Ref { get; private set; }
        public DateTime Arrival { get; private set; }
        public DateTime Departure { get; private set; }
        public int Guests { get; private set; }

        public BookingItem(int refNum, DateTime arrivalDate, DateTime departureDate, int guestsNum)
        {
            this.Ref = refNum;
            this.Arrival = arrivalDate;
            this.Departure = departureDate;
            this.Guests = guestsNum;
        }
    }
}
