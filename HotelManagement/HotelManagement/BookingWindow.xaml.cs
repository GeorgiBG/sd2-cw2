﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;

namespace HotelManagement
{
    public partial class BookingWindow
    {
        int customerReferenceNum;
        int guestCount = 0;

        BookingFacade bookingFacade;
        ExtraItem extra;
        
        public BookingWindow(int customerRef)
        {
            InitializeComponent();

            customerReferenceNum = customerRef;
            bookGuestCountLbl.Content = guestCount.ToString();

            bookingFacade = new BookingFacade();

            ShowCustomerDetails();
            PopulateAgeBox();
        }

        // Leave as it is
        private void ShowCustomerDetails()
        {
            string firstName = DBSingleton.Instance().GetCustomerByReferenceNum(customerReferenceNum).FirstName;
            string lastName = DBSingleton.Instance().GetCustomerByReferenceNum(customerReferenceNum).LastName;

            customerName.Content = "#" + customerReferenceNum.ToString() + " - " + firstName + " " + lastName;
        }

        // Leave as it is
        private void addGuest_Click(object sender, RoutedEventArgs e)
        {
            if (guestCount < 4)
            {
                guestViewDetailsCanvas.Visibility = Visibility.Collapsed;
                guestAddDetailsCanvas.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Cannot add more than 4 guests.");
            }
        }

        // Leave as it is
        private void addGuestCancel_Click(object sender, RoutedEventArgs e)
        {
            // ClearGuestInputs();
            guestAddDetailsCanvas.Visibility = Visibility.Collapsed;
            guestViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void addGuestClear_Click(object sender, RoutedEventArgs e)
        {
            // ClearGuestInputs();
        }

        private void addGuestSave_Click(object sender, RoutedEventArgs e)
        {
            // Create Guest
            if (!arrivalDatePicker.SelectedDate.HasValue || !departureDatePicker.SelectedDate.HasValue)
            {
                MessageBox.Show("Select booking dates.");
                return;
            }

            if (guestPassportNumTxt.Text == string.Empty || guestPassportNumTxt.Text.Length > 10)
            {
                MessageBox.Show("Passport number cannot have more than 10 characters and cannot be left blank as well.");
                return;
            }

            if(guestFirstNameTxt.Text == string.Empty)
            {
                MessageBox.Show("First Name must contain some character input.");
                return;
            }

            if(guestLastNameTxt.Text == string.Empty)
            {
                MessageBox.Show("Last Name must contain some character input.");
                return;
            }

            if(guestAgeBox.SelectedItem == null)
            {
                MessageBox.Show("Age must be specified in order to proceed.");
                return;
            }

            foreach (var checkGuest in bookingFacade.BookingGuestsList())
            {
                if (checkGuest.PassportNumber.Equals(guestPassportNumTxt.Text))
                {
                    MessageBox.Show("Cannot add multiple guests with the same Passport number.");
                    return;
                }
            }

            Guest guest = new Guest(guestFirstNameTxt.Text, guestLastNameTxt.Text, 
                guestPassportNumTxt.Text, byte.Parse(guestAgeBox.SelectedValue.ToString()));

            // Save guest to Collection
            bookingFacade.SaveGuest(guest);
            
            // Update guests counter   
            guestCount++;

            // Display added guests in the listview
            DisplayBookingGuests();

            // Update number of guests label text
            bookGuestCountLbl.Content = guestCount.ToString();
            
            // Hide guest adding form
            guestAddDetailsCanvas.Visibility = Visibility.Collapsed;
            guestViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void makeBooking_Click(object sender, RoutedEventArgs e)
        {
            if (arrivalDatePicker.SelectedDate.HasValue && departureDatePicker.SelectedDate.HasValue)
            {
                if(bookingFacade.BookingGuestsList().Count > 0)
                {
                    bookingFacade.MakeBooking(customerReferenceNum, arrivalDatePicker.SelectedDate.Value, departureDatePicker.SelectedDate.Value, customerNotesTxt.Text);
                    MessageBox.Show("Booking Created.");
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Cannot create the booking without adding any guests first.");
                }
            }
            else
            {
                MessageBox.Show("Select booking dates.");
            }
        }

        private void bookAddBreakfast_Click(object sender, RoutedEventArgs e)
        {
            carHireAddDetailsCanvas.Visibility = Visibility.Collapsed;

            extra = new Breakfast();

            var mealDecors = Enum.GetValues(typeof(MealDecors));
            foreach (var decor in mealDecors)
            {
                string details = decor.ToString(); ;
                if (!extraDecorsListBox.Items.Contains(details))
                    extraDecorsListBox.Items.Add(details);
            }

            extraAddDetailsCanvas.Visibility = Visibility.Visible;
            extraViewDetailsCanvas.Visibility = Visibility.Collapsed;
        }

        private void bookAddDinner_Click(object sender, RoutedEventArgs e)
        {
            carHireAddDetailsCanvas.Visibility = Visibility.Collapsed;

            extra = new Dinner();

            var mealDecors = Enum.GetValues(typeof(MealDecors));
            foreach (var decor in mealDecors)
            {
                string details = decor.ToString(); ;
                if (!extraDecorsListBox.Items.Contains(details))
                    extraDecorsListBox.Items.Add(details);
            }

            extraAddDetailsCanvas.Visibility = Visibility.Visible;
            extraViewDetailsCanvas.Visibility = Visibility.Collapsed;
        }

        private void bookAddCarHire_Click(object sender, RoutedEventArgs e)
        {
            // if CarHires from bookingExtrasList <= 1
            extraViewDetailsCanvas.Visibility = Visibility.Collapsed;
            extraAddDetailsCanvas.Visibility = Visibility.Collapsed;
            carHireAddDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void guestsListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            List<Guest> guestsToSearch = new List<Guest>(bookingFacade.BookingGuestsList());

            string firstName = guestsListBox.SelectedValue.ToString().Split(' ').ElementAt(0);
            string lastName = guestsListBox.SelectedValue.ToString().Split(' ').ElementAt(1);

            string passportNum = guestsToSearch.Find(x => x.FirstName.Equals(firstName) && x.LastName.Equals(lastName)).PassportNumber;

            MessageBox.Show("Passport No: " + passportNum);
        }

        private void extrasListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            List<ExtraItem> extrasToSearch = new List<ExtraItem>(bookingFacade.BookingExtrasList());

            string description = extrasListBox.SelectedValue.ToString();

            decimal cost = extrasToSearch.Find(x => x.Description.Equals(description)).Cost;

            MessageBox.Show("Cost: £" + cost);
        }

        private void extraDecorsListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            extraDecorAmountTxt.Focus();
        }

        private void cancelExtraSave_Click(object sender, RoutedEventArgs e)
        {
            extraAddDetailsCanvas.Visibility = Visibility.Collapsed;
            extraViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void addExtraSave_Click(object sender, RoutedEventArgs e)
        {
            if (guestsListBox.Items.Count == 0)
            {
                MessageBox.Show("You cannot choose meals if you haven't specified any guests.");
                return;
            }

            if (extraDecorsListBox.SelectedItem == null)
            {
                MessageBox.Show("A meal option must be selected from the list in order to proceed.");
                return;
            }

            if (extraDecorAmountTxt.Text == string.Empty)
            {
                MessageBox.Show("Amount for the meal option must be specified.");
                return;
            }

            if (int.Parse(extraDecorAmountTxt.Text) > guestsListBox.Items.Count)
            {
                MessageBox.Show("Cannot add more meals options than the number of guests.");
                return;
            }

            MealTypes mealType = (extra.GetType() == typeof(Breakfast)) ? MealTypes.Breakfast
                : (extra.GetType() == typeof(Dinner)) ? MealTypes.Dinner : MealTypes.Undefined;

            MealDecors mealDecor = (MealDecors)Enum.Parse(typeof(MealDecors), extraDecorsListBox.SelectedValue.ToString(), true);
            
            // Save extra to Collection
            bookingFacade.AddMeal(extra, mealType, mealDecor);

            if (mealType == MealTypes.Breakfast)
                bookingFacade.breakfastsAmount += int.Parse(extraDecorAmountTxt.Text);
            else if (mealType == MealTypes.Dinner)
                bookingFacade.dinnersAmonut += int.Parse(extraDecorAmountTxt.Text);

            DisplayBookingExtras();

            extraAddDetailsCanvas.Visibility = Visibility.Collapsed;
            extraViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void saveCarHire_Click(object sender, RoutedEventArgs e)
        {
            //if (bookingFacade.HireCar(extra, driverNameTxt.Text, carHireStartDatePicker.SelectedDate.Value, carHireEndDatePicker.SelectedDate.Value) != null)
            //{
            if (guestsListBox.Items.Count == 0)
            {
                MessageBox.Show("You cannot hire a car if you haven't specified any guests.");
                return;
            }

            if(arrivalDatePicker.SelectedDate == null || departureDatePicker.SelectedDate == null)
            {
                MessageBox.Show("You must first select arrival and departure dates for the booking in order to be able to specify car hiring dates.");
                return;
            }

            if (carHireStartDatePicker.SelectedDate == null)
            {
                MessageBox.Show("You haven't specified a hire start date.");
                return;
            }

            if(carHireEndDatePicker.SelectedDate == null)
            {
                MessageBox.Show("You haven't specified a hire end date.");
                return;
            }

            if(DateTime.Compare(carHireStartDatePicker.SelectedDate.Value, carHireEndDatePicker.SelectedDate.Value) >= 0)
            {
                MessageBox.Show("The specified dates are invalid. Hire start date cannot be later than Hire end date.");
                return;
            }

            if(DateTime.Compare(carHireStartDatePicker.SelectedDate.Value, arrivalDatePicker.SelectedDate.Value) < 0 
                || DateTime.Compare(carHireEndDatePicker.SelectedDate.Value, departureDatePicker.SelectedDate.Value) > 0)
            {
                MessageBox.Show("The specified dates are invalid. Check the time interval.");
                return;
            }

            if(driverNameTxt.Text == string.Empty)
            {
                MessageBox.Show("You must include a driver's name in order to proceed.");
                return;
            }

            CarHire car = bookingFacade.HireCar(extra, driverNameTxt.Text, carHireStartDatePicker.SelectedDate.Value, carHireEndDatePicker.SelectedDate.Value);

            if (!extrasListBox.Items.Contains(car.Description))
            {
                extrasListBox.Items.Add(car.Description);
            }
            else
            {
                MessageBox.Show("Cannot add more car hires.");
                return;
            }

            carHireAddDetailsCanvas.Visibility = Visibility.Collapsed;
            extraViewDetailsCanvas.Visibility = Visibility.Visible;
            //}
            //else
            //{
                //MessageBox.Show("Dates are invalid.");
            //}
        }

        private void cancelCarHire_Click(object sender, RoutedEventArgs e)
        {
            carHireAddDetailsCanvas.Visibility = Visibility.Collapsed;
            extraViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void removeSelectedGuest_Click(object sender, RoutedEventArgs e)
        {
            // TODO
        }

        private void removeSelectedExtra_Click(object sender, RoutedEventArgs e)
        {
            // TODO
        }


        /*** HELPER METHODS  ***/
        private void DisplayBookingGuests()
        {
            foreach (var guest in bookingFacade.BookingGuestsList())
            {
                string details = guest.FirstName + " " + guest.LastName;
                if (!guestsListBox.Items.Contains(details))
                    guestsListBox.Items.Add(details);
            }
        }

        private void DisplayBookingExtras()
        {
            foreach (var extra in bookingFacade.BookingExtrasList())
            {
                if (!extrasListBox.Items.Contains(extra.Description))
                    extrasListBox.Items.Add(extra.Description);
            }
        }

        private void PopulateAgeBox()
        {
            for(int i = 0; i < 100; i++)
                guestAgeBox.Items.Add(i.ToString());
        }
    }
}