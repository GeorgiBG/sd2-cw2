﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace HotelManagement
{
    class Booking
    {
        #region CLASS PROPERTIES
        private int bookingRef;
        private DateTime arrivalDate;
        private DateTime departureDate;

        public List<ExtraItem> extras;
        public List<Guest> guests;

        private DBSingleton Database = DBSingleton.Instance();
        #endregion

        #region GETTERS AND SETTERS
        public int BookingRef
        {
            get
            {
                return bookingRef;
            }
            set
            {
                bookingRef = value;
            }
        }

        public DateTime ArrivalDate
        {
            get
            {
                return arrivalDate;
            }
            set
            {
                arrivalDate = value;
            }
        }

        public DateTime DepartureDate
        {
            get
            {
                return departureDate;
            }
            set
            {
                departureDate = value;
            }
        }
        #endregion

        // NO EMPTY CONSTRUCTOR! MUST DEFINE A CUSTOMER FIRST!
        public Booking()
        {
            if (Database.GetLastBookingReferenceNumber() > 0)
                BookingRef = Database.GetLastBookingReferenceNumber() + 1;
            else
                BookingRef = 1;
        }

        public void AddNewBooking(int customerRef, string customerNotes)
        {
            // Save the whole thing to DB Booking tbl
            Dictionary<string, string> bookingDetails = new Dictionary<string, string>();

            bookingDetails.Add("ReferenceNumber", BookingRef.ToString());
            bookingDetails.Add("CustomerReferenceNumber", customerRef.ToString());
            bookingDetails.Add("NumberOfGuests", guests.Count.ToString());
            bookingDetails.Add("ArrivalDate", ArrivalDate.ToShortDateString());
            bookingDetails.Add("DepartureDate", DepartureDate.ToShortDateString());
            bookingDetails.Add("CustomerNotes", customerNotes);
            // Add to DB
            Database.SetBooking(bookingDetails);
        }

        public void AddBookingGuests(string guestPassNum)
        {
            Dictionary<string, string> bookingGuestDetails = new Dictionary<string, string>();

            bookingGuestDetails.Add("Id", (Database.GetLastBookingGuestId() > 0 ? (Database.GetLastBookingGuestId() + 1).ToString() : 1.ToString()));
            bookingGuestDetails.Add("BookingReferenceNumber", BookingRef.ToString());
            bookingGuestDetails.Add("GuestPassportNumber", guestPassNum);

            // Add to DB
            Database.SetBookingGuest(bookingGuestDetails);
        }

        public void AddBookingExtras()
        {
            Dictionary<string, string> bookingExtraDetails = new Dictionary<string, string>();

            bookingExtraDetails.Add("Id", (Database.GetLastBookingExtraId() > 0 ? (Database.GetLastBookingExtraId() + 1).ToString() : 1.ToString()));
            bookingExtraDetails.Add("BookingReferenceNumber", BookingRef.ToString());
            bookingExtraDetails.Add("ExtraId", Database.GetLastExtraId().ToString());

            // Add to DB
            Database.SetBookingExtra(bookingExtraDetails);
        }

        public void SetBookingExtrasAmount(int breakfastsAmount, int dinnersAmount, int carhireAmount)
        {
            Dictionary<string, string> bookingDetails = new Dictionary<string, string>();

            bookingDetails.Add("BookingReferenceNumber", BookingRef.ToString());
            bookingDetails.Add("Breakfasts", breakfastsAmount.ToString());
            bookingDetails.Add("Dinners", dinnersAmount.ToString());
            bookingDetails.Add("CarHires", carhireAmount.ToString());

            // Add to DB
            Database.SetBookingExtrasAmount(bookingDetails);
        }
    }
}
 