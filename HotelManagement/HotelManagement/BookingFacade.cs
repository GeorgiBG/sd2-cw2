﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace HotelManagement // Enumerators
{
    public enum MealTypes
    {
        Undefined,
        Breakfast,
        Dinner
    }

    public enum MealDecors
    {
        Normal,
        Vegetarian,
        NutFree
    }
}

namespace HotelManagement
{
    class BookingFacade
    {
        Booking booking;

        DBSingleton Database = DBSingleton.Instance();

        public int breakfastsAmount = 0;
        public int dinnersAmonut = 0;

        // Constructors
        public BookingFacade()
        {
            booking = new Booking();
            booking.guests = new List<Guest>();
            booking.extras = new List<ExtraItem>();
        }

        public void MakeBooking(int customerRef, DateTime arrivalDate, DateTime departureDate, string notes)
        {
            decimal payment = 0;
            // Add booking details to DB
            if (booking.guests != null)
            {
                SetBookingDates(arrivalDate, departureDate);
                booking.AddNewBooking(customerRef, notes);

                decimal totalDays = decimal.Parse((departureDate - arrivalDate).TotalDays.ToString());

                foreach (var guest in booking.guests)
                {
                    guest.AddNewGuest();
                    booking.AddBookingGuests(guest.PassportNumber);

                    if (guest.Age < 18)
                        payment += 30 * totalDays;
                    else
                        payment += 50 * totalDays;
                }

                int carCount = 0;
                if (booking.extras != null)
                {
                    foreach (var extra in booking.extras)
                    {
                        if (extra.Description.Contains("Car"))
                        {
                            CarHire carExtra = (CarHire)extra;
                            carExtra.AddNewExtra();
                            carCount++;
                            payment += carExtra.Cost;
                        }
                        else
                        {
                            extra.AddNewExtra();
                        }

                        booking.AddBookingExtras();
                    }

                    payment += breakfastsAmount * 5 * totalDays;
                    payment += dinnersAmonut * 15 * totalDays;
                }

                booking.SetBookingExtrasAmount(breakfastsAmount, dinnersAmonut, carCount);
                Database.SetBookingPayment(booking.BookingRef, payment);
            }
            else
            {
                MessageBox.Show("Cannot finalize booking. Missing details.");
            }
        }

        public void UpdateBooking(int bookingRef, DateTime arrivalDate, DateTime departureDate, string notes)
        {
            decimal payment = 0;

            SetBookingDates(arrivalDate, departureDate);
            decimal totalDays = decimal.Parse((departureDate - arrivalDate).TotalDays.ToString());

            // CALCULATE POSSIBLE UPDATED PAYMENT
            foreach (var guest in booking.guests)
            {
                if (guest.Age < 18)
                    payment += 30 * totalDays;
                else
                    payment += 50 * totalDays;
            }

            var carExtra = booking.extras.Find(x => x.Description.Contains("Car"));
            if(carExtra != null)
            {
                CarHire carHire = (CarHire)carExtra;
                payment += carHire.Cost;
            }

            payment += breakfastsAmount * 5 * totalDays;
            payment += dinnersAmonut * 15 * totalDays;

            Dictionary<string, string> bookingDetails = new Dictionary<string, string>();
            bookingDetails.Add("BookingRef", bookingRef.ToString());
            bookingDetails.Add("ArrivalDate", booking.ArrivalDate.ToShortDateString());
            bookingDetails.Add("DepartureDate", booking.DepartureDate.ToShortDateString());
            bookingDetails.Add("NumOfGuests", booking.guests.Count.ToString());
            bookingDetails.Add("CustomerNotes", notes);
            bookingDetails.Add("Payment", payment.ToString());
            Database.UpdateBooking(bookingDetails);
        }

        public void AddMeal(ExtraItem meal, MealTypes type, MealDecors decor)
        {
            if (booking.guests != null && booking.guests.Count > 0)
            {
                if (meal != null)
                {
                    if (type == MealTypes.Undefined)
                        // throw and exit with error message;
                        return;
                    else if (type == MealTypes.Breakfast)
                        meal = decoratedMeal(new Breakfast(), decor);
                    else if (type == MealTypes.Dinner)
                        meal = decoratedMeal(new Dinner(), decor);
                    else
                        // throw and exit with error message;
                        return;

                    booking.extras.Add(meal);
                }
                else
                {
                    // throw and exit with error message;
                    return;
                }
            }
            else
            {
                MessageBox.Show("Add some guests.");
            }
        }

        public CarHire HireCar(ExtraItem carHire, string driverName, DateTime hireStartDate, DateTime hireEndDate)
        {
            if (booking.guests != null && booking.guests.Count > 0)
            {
                int totalCarHires = booking.extras.FindAll(x => x.Description.Contains("Car")).Count;

                carHire = new CarHire();
                CarHire car = (CarHire)carHire;

                if (car != null)
                {
                    //if (CarHireDatesAreValid(hireStartDate, hireEndDate))
                    //{
                        car.ScheduleDates(hireStartDate, hireEndDate);
                        car.DriverName = driverName;
                        car.CalculateCost();
                        booking.extras.Add(car);
                        return car;
                    //}
                    //else
                    //{
                        //return null;
                    //}
                }
                else
                {
                    // throw and exit with error message;
                    return null;
                }
            }
            else
            {
                MessageBox.Show("Add some guests.!!");
                return null;
            }
        }

        public void SaveGuest(Guest guest)
        {
            booking.guests.Add(guest);
        }

        public void SaveExtra(ExtraItem extra)
        {
            booking.extras.Add(extra);
        }

        public void UpdateGuestsList(Guest guest)
        {
            booking.guests.Add(guest);
        }

        public void DeleteGuestFromList(int position)
        {
            booking.guests.RemoveAt(position);
        }

        public void UpdateExtrasList(ExtraItem extra)
        {
            booking.extras.Add(extra);
        }

        public HashSet<Guest> BookingGuestsList()
        {
            HashSet<Guest> guestsList = new HashSet<Guest>();
            foreach (var guest in booking.guests)
            {
                if(guest != null)
                    guestsList.Add(guest);
            }
            return guestsList;
        }

        public HashSet<ExtraItem> BookingExtrasList()
        {
            HashSet<ExtraItem> extrasList = new HashSet<ExtraItem>();
            foreach (var extra in booking.extras)
            {
                if (extra != null)
                    extrasList.Add(extra);
            }
            return extrasList;
        }

        private void SetBookingDates(DateTime arrivalDate, DateTime departureDate)
        {
            booking.ArrivalDate = arrivalDate;
            booking.DepartureDate = departureDate;
        }

        #region HELPER METHODS
        // ---------------- //
        /** HELPER METHODS **/
        // ---------------- //

        private ExtraItem decoratedMeal(ExtraItem meal, MealDecors decor)
        {
            if (decor == MealDecors.Normal)
                return meal;

            ExtraItem decoratedMeal = meal;

            if (decor == MealDecors.Vegetarian)
                decoratedMeal = new Vegetarian(meal);

            if (decor == MealDecors.NutFree)
                decoratedMeal = new NutFree(meal);

            return decoratedMeal;
        }

        private bool CarHireDatesAreValid(DateTime hireStartDate, DateTime hireEndDate)
        {
            return DateTime.Compare(hireStartDate, booking.ArrivalDate) >= 0 && DateTime.Compare(hireEndDate, booking.DepartureDate) <= 0;
        }
        #endregion
    }
}