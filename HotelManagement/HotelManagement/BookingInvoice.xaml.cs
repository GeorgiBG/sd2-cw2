﻿using System;
using System.Collections.Generic;

namespace HotelManagement
{
    public partial class BookingInvoice
    {
        int bookingReferenceNum;

        private DBSingleton Database = DBSingleton.Instance();

        Dictionary<string, string> invoiceDetails;

        public BookingInvoice(int bookingRef)
        {
            InitializeComponent();

            bookingReferenceNum = bookingRef;

            invoiceDetails = new Dictionary<string, string>(Database.GetBookingInvoice(bookingReferenceNum));

            var customer = Database.GetCustomerByBookingReference(bookingReferenceNum);

            invoiceTitle.Content = "#" + bookingReferenceNum + " / "
                + customer.FirstName + " " + customer.LastName + " / "
                + "(" + DateTime.Parse(invoiceDetails["ArrivalDate"]).ToShortDateString() 
                + "-" + DateTime.Parse(invoiceDetails["DepartureDate"]).ToShortDateString() + ")";

            foreach(var extraInvoice in invoiceDetails)
            {
                if (extraInvoice.Key.Contains("Total") || extraInvoice.Key.Contains("Date")) continue;
                listBox.Items.Add(extraInvoice.Value);
            }

            GuestsTotal.Content = "Number of guests: " + invoiceDetails["TotalGuests"];
            BreakfastTotal.Content = "Breakfast total: £" + invoiceDetails["BreakfastTotal"];
            DinnerTotal.Content = "Diner total: £" + invoiceDetails["DinnerTotal"];
            CarHireTotal.Content = "Car hire total: £" + invoiceDetails["CarHireTotal"];
            BookingTotal.Content = "Booking total: £" + invoiceDetails["TotalPayment"];
        }
    }
}
