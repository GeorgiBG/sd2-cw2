﻿using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System;
using System.Configuration;
using System.Windows.Controls;
using System.Data;

namespace HotelManagement
{
    public partial class MainWindow
    {
        private DBSingleton Database = DBSingleton.Instance();
        private int customerRefToPass = 0;

        public MainWindow()
        {
            InitializeComponent();
            UpdateCustomersDataGrid(CustCustomersDataGrid);
        }

        private void CustomerAddBtn_Click(object sender, RoutedEventArgs e)
        {
            if(CustFirstNameTxtBox.Text == string.Empty)
            {
                MessageBox.Show("First Name must contain some character input.");
                return;
            }

            if (CustLastNameTxtBox.Text == string.Empty)
            {
                MessageBox.Show("Last Name must contain some character input.");
                return;
            }

            if (CustAddressTxtBox.Text == string.Empty)
            {
                MessageBox.Show("Address must contain some character input.");
                return;
            }

            Customer customer = new Customer(CustFirstNameTxtBox.Text, CustLastNameTxtBox.Text, CustAddressTxtBox.Text);
            customer.AddNewCustomer();

            UpdateCustomersDataGrid(CustCustomersDataGrid);
        }

        private void bookFind_Click(object sender, RoutedEventArgs e)
        {
            if (BookCustomerSearchCombo.SelectedIndex > -1)
            {
                ComboBoxItem searchComboItem = (ComboBoxItem)BookCustomerSearchCombo.SelectedItem;
                string customerSearchTerm = searchComboItem.Content.ToString();

                if (customerSearchTerm.Equals("All"))
                {
                    UpdateCustomersDataGrid(BookCustomersDataGrid);
                }
                else if (customerSearchTerm.Equals("Reference"))
                {
                    if (BookCustomerSearchTxt.Text != string.Empty)
                        UpdateCustomersDataGridByReference(BookCustomersDataGrid, int.Parse(BookCustomerSearchTxt.Text));
                    else
                        MessageBox.Show("Enter customer reference number.");
                }
                else if (customerSearchTerm.Equals("Name"))
                {
                    if (BookCustomerSearchTxt.Text != string.Empty)
                        UpdateCustomersDataGridByName(BookCustomersDataGrid, BookCustomerSearchTxt.Text);
                    else
                        MessageBox.Show("Enter customer name.");
                }
                else if (customerSearchTerm.Equals("Address"))
                {
                    if (BookCustomerSearchTxt.Text != string.Empty)
                        UpdateCustomersDataGridByAddress(BookCustomersDataGrid, BookCustomerSearchTxt.Text);
                    else
                        MessageBox.Show("Enter customer address.");
                }
            }
            else
            {
                UpdateCustomersDataGrid(BookCustomersDataGrid);
            }
        }

        private void bookCreate_Click(object sender, RoutedEventArgs e)
        {
            if (customerRefToPass != 0)
            {
                BookingWindow bookingWindow = new BookingWindow(customerRefToPass);
                bookingWindow.ShowDialog();
            }
        }

        private void bookView_Click(object sender, RoutedEventArgs e)
        {
            if (customerRefToPass != 0)
            {
                CustomerBookingsView customerBookingsWindow = new CustomerBookingsView(customerRefToPass);
                customerBookingsWindow.ShowDialog();
            }
        }

        private void BookCustomersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BookCustomersDataGrid.SelectedItem == null) return;
            CustomerItem custRow = BookCustomersDataGrid.SelectedItem as CustomerItem;
            customerRefToPass = custRow.Ref;
        }

        private void custFind_Click(object sender, RoutedEventArgs e)
        {
            if (CustSearchCustomerCombo.SelectedIndex > -1)
            {
                ComboBoxItem searchComboItem = (ComboBoxItem)CustSearchCustomerCombo.SelectedItem;
                string customerSearchTerm = searchComboItem.Content.ToString();

                if (customerSearchTerm.Equals("All"))
                {
                    UpdateCustomersDataGrid(CustCustomersDataGrid);
                }
                else if (customerSearchTerm.Equals("Reference"))
                {
                    if (CustSearchTxt.Text != string.Empty)
                        UpdateCustomersDataGridByReference(CustCustomersDataGrid, int.Parse(CustSearchTxt.Text));
                    else
                        MessageBox.Show("Enter customer reference number.");
                }
                else if (customerSearchTerm.Equals("Name"))
                {
                    if (CustSearchTxt.Text != string.Empty)
                        UpdateCustomersDataGridByName(CustCustomersDataGrid, CustSearchTxt.Text);
                    else
                        MessageBox.Show("Enter customer name.");
                }
                else if (customerSearchTerm.Equals("Address"))
                {
                    if (CustSearchTxt.Text != string.Empty)
                        UpdateCustomersDataGridByAddress(CustCustomersDataGrid, CustSearchTxt.Text);
                    else
                        MessageBox.Show("Enter customer address.");
                }
            }
            else
            {
                UpdateCustomersDataGrid(CustCustomersDataGrid);
            }
        }

        private void CustCustomersDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CustCustomersDataGrid.SelectedItem == null) return;
            CustomerItem custRow = CustCustomersDataGrid.SelectedItem as CustomerItem;
            customerRefToPass = custRow.Ref;

            CustomerDetailsWindow customerWindow = new CustomerDetailsWindow(customerRefToPass);
            customerWindow.ShowDialog();
        }

        /*** HELPERS ***/
        private void UpdateCustomersDataGrid(DataGrid dataGrid)
        {
            List<NH_Customer> allCustomers = Database.GetAllCustomers();

            HashSet<CustomerItem> customerItems = new HashSet<CustomerItem>();
            foreach (var cust in allCustomers)
            {
                CustomerItem custItem = new CustomerItem(cust.ReferenceNum, cust.FirstName, cust.LastName, cust.Address);
                if (!customerItems.Contains(custItem))
                    customerItems.Add(custItem);
            }

            dataGrid.ItemsSource = customerItems;
        }

        private void UpdateCustomersDataGridByReference(DataGrid dataGrid, int referenceNumTerm)
        {
            NH_Customer matchCustomer = Database.GetCustomerByReferenceNum(referenceNumTerm);
            if(matchCustomer != null)
            {
                HashSet<CustomerItem> customerItems = new HashSet<CustomerItem>();
                customerItems.Add(new CustomerItem(matchCustomer.ReferenceNum, matchCustomer.FirstName, matchCustomer.LastName, matchCustomer.Address));
                dataGrid.ItemsSource = customerItems;
            }
            else
            {
                MessageBox.Show("Such customer does not exist.");
            }
        }

        private void UpdateCustomersDataGridByName(DataGrid dataGrid, string nameTerm)
        {
            List<NH_Customer> allCustomers = Database.GetAllCustomersByName(nameTerm);

            HashSet<CustomerItem> customerItems = new HashSet<CustomerItem>();
            foreach (var cust in allCustomers)
            {
                CustomerItem custItem = new CustomerItem(cust.ReferenceNum, cust.FirstName, cust.LastName, cust.Address);
                if (!customerItems.Contains(custItem))
                    customerItems.Add(custItem);
            }

            dataGrid.ItemsSource = customerItems;
        }

        private void UpdateCustomersDataGridByAddress(DataGrid dataGrid, string addressTerm)
        {
            List<NH_Customer> allCustomers = Database.GetAllCustomersByAddress(addressTerm);

            HashSet<CustomerItem> customerItems = new HashSet<CustomerItem>();
            foreach (var cust in allCustomers)
            {
                CustomerItem custItem = new CustomerItem(cust.ReferenceNum, cust.FirstName, cust.LastName, cust.Address);
                if (!customerItems.Contains(custItem))
                    customerItems.Add(custItem);
            }

            dataGrid.ItemsSource = customerItems;
        }

        private void CustCustomersDataGrid_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {

        }
    }

    class CustomerItem
    {
        public int Ref { get; private set; }
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string Address { get; private set; }

        public CustomerItem(int refNum, string firstName, string lastName, string address)
        {
            this.Ref = refNum;
            this.Name = firstName;
            this.Surname = lastName;
            this.Address = address;
        }
    }
}