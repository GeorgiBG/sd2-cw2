﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace HotelManagement
{
    class Guest : Person
    {
        private string passportNumber;
        private byte age;
        private DBSingleton Database = DBSingleton.Instance();
        
        #region GETTERS/SETTERS
        public string PassportNumber
        {
            get
            {
                return passportNumber;
            }
            set
            {
                if (value != string.Empty && value.Length <= 10)
                    passportNumber = value;
                else
                    throw new ArgumentException("Cannot be empty OR more than 10 chars.");
            }
        }

        public byte Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
            }
        }
        #endregion
        public Guest(string firstName, string lastName) : base(firstName, lastName)
        { }

        public Guest(string firstName, string lastName, string passportNumber, byte age) : base(firstName, lastName)
        {
            try
            {
                PassportNumber = passportNumber;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }

            Age = age;
        }

        public void AddNewGuest()
        {
            Dictionary<string, string> guestDetails = new Dictionary<string, string>();

            guestDetails.Add("GuestPassportNumber", PassportNumber);
            guestDetails.Add("FirstName", FirstName);
            guestDetails.Add("LastName", LastName);
            guestDetails.Add("Age", Age.ToString());
            // Add to DB
            Database.SetGuest(guestDetails);
        }
    }
}
