﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace HotelManagement
{
    public partial class BookingDetailsWindow
    {
        int bookingReferenceNumber;

        int guestCount = 0;

        string passportNumToDel = string.Empty;
        int extraIdToDel = 0;

        private DBSingleton Database = DBSingleton.Instance();

        private BookingFacade bookingFacade;
        ExtraItem extra;
        
        public BookingDetailsWindow(int bookingRef)
        {
            InitializeComponent();

            bookingFacade = new BookingFacade();

            bookingReferenceNumber = bookingRef;

            ShowBookingDetails();

            PopulateAgeBox();
        }

        private void editGuestsListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (editGuestsListBox.SelectedItem == null) return;

            List<Guest> guestsToSearch = new List<Guest>(bookingFacade.BookingGuestsList());

            string firstName = editGuestsListBox.SelectedValue.ToString().Split(' ').ElementAt(0);
            string lastName = editGuestsListBox.SelectedValue.ToString().Split(' ').ElementAt(1);

            string passportNum = guestsToSearch.Find(x => x.FirstName.Equals(firstName) && x.LastName.Equals(lastName)).PassportNumber;

            MessageBox.Show("Passport number: " + passportNum);

            passportNumToDel = passportNum;
        }

        private void editExtrasListBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (editGuestsListBox.SelectedItem == null) return;

            List<ExtraItem> extrasToSearch = new List<ExtraItem>(bookingFacade.BookingExtrasList());

            string description = editExtrasListBox.SelectedValue.ToString();

            decimal cost = extrasToSearch.Find(x => x.Description.Equals(description)).Cost;
            int countNum = extrasToSearch.FindAll(x => x.Description.Equals(description)).Count;

            MessageBox.Show("Cost: £" + cost + " (each)" + Environment.NewLine + "Amount: " + countNum);
        }

        /** HELPER METHODS **/
        private void ShowBookingDetails()
        {
            editBookingTitle.Content = "#" + bookingReferenceNumber.ToString();

            editArrivalDatePicker.SelectedDate = Database.GetBookingDates(bookingReferenceNumber).Item1;
            editDepartureDatePicker.SelectedDate = Database.GetBookingDates(bookingReferenceNumber).Item2;
            editCustomerNotesTxt.Text = Database.GetBookingNotes(bookingReferenceNumber);

            UpdateGuestsList();
            UpdateExtrasList();

            DisplayGuests();
            DisplayExtras();

            PopulateAgeBox();
        }

        private void UpdateGuestsList()
        {
            var bookingGuests = Database.GetAllGuestsByBookingRef(bookingReferenceNumber);
            foreach (var guest in bookingGuests)
            {
                Guest newGuest = new Guest(guest.FirstName, guest.LastName, guest.PassportNum, guest.Age);
                bookingFacade.UpdateGuestsList(newGuest);
                guestCount++;
                editBookGuestCountLbl.Content = guestCount.ToString();
            }
        }

        private void UpdateExtrasList()
        {
            var bookingExtras = Database.GetAllExtrasByBookingRef(bookingReferenceNumber);
            foreach (var extra in bookingExtras)
            {
                if (extra != null)
                {
                    if (extra.Description.Contains("Breakfast"))
                    {
                        Breakfast breakfast = new Breakfast();
                        breakfast.Description = extra.Description;
                        breakfast.Cost = extra.Cost;

                        bookingFacade.UpdateExtrasList(breakfast);
                    }
                    else if (extra.Description.Contains("Dinner"))
                    {
                        Dinner dinner = new Dinner();
                        dinner.Description = extra.Description;
                        dinner.Cost = extra.Cost;

                        bookingFacade.UpdateExtrasList(dinner);
                    }
                    else if (extra.Description.Contains("Car"))
                    {
                        int carHireId = extra.CarHireId ?? default(int);
                        var carHireData = Database.GetCarHireById(carHireId);

                        CarHire carHire = new CarHire();
                        carHire.Description = extra.Description;
                        carHire.Cost = extra.Cost;
                        carHire.DriverName = carHireData.Driver;
                        carHire.HireStartDate = carHireData.StartDate;
                        carHire.HireEndDate = carHireData.EndDate;

                        bookingFacade.UpdateExtrasList(carHire);
                    }
                    else
                    {
                        MessageBox.Show("Invalid extra type.");
                    }
                }
                else
                {
                    MessageBox.Show("No such extra OR no extra is selected.");
                }
            }
        }

        private void DisplayGuests()
        {
            foreach (var guest in bookingFacade.BookingGuestsList())
            {
                if (!editGuestsListBox.Items.Contains(guest.FirstName + " " + guest.LastName))
                    editGuestsListBox.Items.Add(guest.FirstName + " " + guest.LastName);
            }
        }

        private void DisplayExtras()
        {
            foreach (var extra in bookingFacade.BookingExtrasList())
            {
                if (!editExtrasListBox.Items.Contains(extra.Description))
                    editExtrasListBox.Items.Add(extra.Description);
            }
        }

        private void PopulateAgeBox()
        {
            for (int i = 0; i < 100; i++)
                editGuestAgeBox.Items.Add(i.ToString());
        }

        private void editBookAddBreakfast_Click(object sender, RoutedEventArgs e)
        {
            editCarHireAddDetailsCanvas.Visibility = Visibility.Collapsed;

            extra = new Breakfast();

            var mealDecors = Enum.GetValues(typeof(MealDecors));
            foreach (var decor in mealDecors)
            {
                string details = decor.ToString(); ;
                if (!editExtraDecorsListBox.Items.Contains(details))
                    editExtraDecorsListBox.Items.Add(details);
            }

            editExtraAddDetailsCanvas.Visibility = Visibility.Visible;
            editExtraViewDetailsCanvas.Visibility = Visibility.Collapsed;
        }

        private void editBookAddDinner_Click(object sender, RoutedEventArgs e)
        {
            editCarHireAddDetailsCanvas.Visibility = Visibility.Collapsed;

            extra = new Dinner();

            var mealDecors = Enum.GetValues(typeof(MealDecors));
            foreach (var decor in mealDecors)
            {
                string details = decor.ToString(); ;
                if (!editExtraDecorsListBox.Items.Contains(details))
                    editExtraDecorsListBox.Items.Add(details);
            }

            editExtraAddDetailsCanvas.Visibility = Visibility.Visible;
            editExtraViewDetailsCanvas.Visibility = Visibility.Collapsed;
        }

        private void editBookAddCarHire_Click(object sender, RoutedEventArgs e)
        {
            // if CarHires from bookingExtrasList <= 1
            editExtraViewDetailsCanvas.Visibility = Visibility.Collapsed;
            editExtraAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editCarHireAddDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void editAddGuest_Click(object sender, RoutedEventArgs e)
        {
            if (guestCount < 4)
            {
                editGuestViewDetailsCanvas.Visibility = Visibility.Collapsed;
                editGuestAddDetailsCanvas.Visibility = Visibility.Visible;
            }
            else
            {
                MessageBox.Show("Cannot add more than 4 guests.");
            }
        }

        private void editRemoveSelectedGuest_Click(object sender, RoutedEventArgs e)
        {
            if(passportNumToDel != string.Empty)
            {
                editGuestsListBox.Items.Remove(editGuestsListBox.SelectedItem);
                Database.DeleteBookingGuest(bookingReferenceNumber, passportNumToDel);
                guestCount--;
            }
            else
            {
                MessageBox.Show("No guests selected to delete.");
            }
        }

        private void editRemoveSelectedExtra_Click(object sender, RoutedEventArgs e)
        {
            /*
            MessageBox.Show(extraIdToDel.ToString());
            if(extraIdToDel != 0)
                Database.DeleteBookingExtra(bookingReferenceNumber, extraIdToDel);
            */
            MessageBox.Show("In development...");
        }

        private void editAddGuestSave_Click(object sender, RoutedEventArgs e)
        {
            // Create Guest
            if (editGuestPassportNumTxt.Text == string.Empty || editGuestPassportNumTxt.Text.Length > 10)
            {
                MessageBox.Show("Passport number cannot have more than 10 characters and cannot be left blank as well.");
                return;
            }

            if (editGuestFirstNameTxt.Text == string.Empty)
            {
                MessageBox.Show("First Name must contain some character input.");
                return;
            }

            if (editGuestLastNameTxt.Text == string.Empty)
            {
                MessageBox.Show("Last Name must contain some character input.");
                return;
            }

            if (editGuestAgeBox.SelectedItem == null)
            {
                MessageBox.Show("Age must be specified in order to proceed.");
                return;
            }

            foreach (var checkGuest in bookingFacade.BookingGuestsList())
            {
                if (checkGuest.PassportNumber.Equals(editGuestPassportNumTxt.Text))
                {
                    MessageBox.Show("Cannot add multiple guests with the same Passport number.");
                    return;
                }
            }

            Guest guest = new Guest(editGuestFirstNameTxt.Text, editGuestLastNameTxt.Text,
                editGuestPassportNumTxt.Text, byte.Parse(editGuestAgeBox.SelectedValue.ToString()));

            // Save guest to Collection
            bookingFacade.SaveGuest(guest);

            // Update guests counter   
            guestCount++;

            // Display added guests in the listview
            DisplayGuests();

            // Update number of guests label text
            editBookGuestCountLbl.Content = guestCount.ToString();

            // Hide guest adding form
            editGuestAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editGuestViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void editAddGuestCancel_Click(object sender, RoutedEventArgs e)
        {
            // ClearGuestInputs();
            editGuestAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editGuestViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void editAddGuestClear_Click(object sender, RoutedEventArgs e)
        {
            // TODO
        }

        private void editAddExtraSave_Click(object sender, RoutedEventArgs e)
        {
            if (editGuestsListBox.Items.Count == 0)
            {
                MessageBox.Show("You cannot choose meals if you haven't specified any guests.");
                return;
            }

            if (editExtraDecorsListBox.SelectedItem == null)
            {
                MessageBox.Show("A meal option must be selected from the list in order to proceed.");
                return;
            }

            if (editExtraDecorAmountTxt.Text == string.Empty)
            {
                MessageBox.Show("Amount for the meal option must be specified.");
                return;
            }

            if (int.Parse(editExtraDecorAmountTxt.Text) > editGuestsListBox.Items.Count)
            {
                MessageBox.Show("Cannot add more meals options than the number of guests.");
                return;
            }

            MealTypes mealType = (extra.GetType() == typeof(Breakfast)) ? MealTypes.Breakfast
                : (extra.GetType() == typeof(Dinner)) ? MealTypes.Dinner : MealTypes.Undefined;

            MealDecors mealDecor = (MealDecors)Enum.Parse(typeof(MealDecors), editExtraDecorsListBox.SelectedValue.ToString(), true);
                
            // Save extra to Collection
            bookingFacade.AddMeal(extra, mealType, mealDecor);

            DisplayExtras();

            editExtraAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editExtraViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void editCancelExtraSave_Click(object sender, RoutedEventArgs e)
        {
            editExtraAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editExtraViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void editSaveCarHire_Click(object sender, RoutedEventArgs e)
        {
            //if(bookingFacade.HireCar(extra, editDriverNameTxt.Text, editCarHireStartDatePicker.SelectedDate.Value, editCarHireEndDatePicker.SelectedDate.Value) != null)
            //{
            if (editGuestsListBox.Items.Count == 0)
            {
                MessageBox.Show("You cannot hire a car if you haven't specified any guests.");
                return;
            }

            if (editArrivalDatePicker.SelectedDate == null || editDepartureDatePicker.SelectedDate == null)
            {
                MessageBox.Show("You must first select arrival and departure dates for the booking in order to be able to specify car hiring dates.");
                return;
            }

            if (editCarHireStartDatePicker.SelectedDate == null)
            {
                MessageBox.Show("You haven't specified a hire start date.");
                return;
            }

            if (editCarHireEndDatePicker.SelectedDate == null)
            {
                MessageBox.Show("You haven't specified a hire end date.");
                return;
            }

            if (DateTime.Compare(editCarHireStartDatePicker.SelectedDate.Value, editCarHireEndDatePicker.SelectedDate.Value) >= 0)
            {
                MessageBox.Show("The specified dates are invalid. Hire start date cannot be later than Hire end date.");
                return;
            }

            if (DateTime.Compare(editCarHireStartDatePicker.SelectedDate.Value, editArrivalDatePicker.SelectedDate.Value) < 0
                || DateTime.Compare(editCarHireEndDatePicker.SelectedDate.Value, editDepartureDatePicker.SelectedDate.Value) > 0)
            {
                MessageBox.Show("The specified dates are invalid. Check the time interval.");
                return;
            }

            if (editDriverNameTxt.Text == string.Empty)
            {
                MessageBox.Show("You must include a driver's name in order to proceed.");
                return;
            }

            CarHire car = bookingFacade.HireCar(extra, editDriverNameTxt.Text, editCarHireStartDatePicker.SelectedDate.Value, editCarHireEndDatePicker.SelectedDate.Value);

            if (!editExtrasListBox.Items.Contains(car.Description))
                editExtrasListBox.Items.Add(car.Description);

            editCarHireAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editExtraViewDetailsCanvas.Visibility = Visibility.Visible;
            //}
            //else
            //{
                //MessageBox.Show("Dates are invalid.");
            //}
        }

        private void editCancelCarHire_Click(object sender, RoutedEventArgs e)
        {
            editCarHireAddDetailsCanvas.Visibility = Visibility.Collapsed;
            editExtraViewDetailsCanvas.Visibility = Visibility.Visible;
        }

        private void updateBooking_Click(object sender, RoutedEventArgs e)
        {
            bookingFacade.UpdateBooking(bookingReferenceNumber, editArrivalDatePicker.SelectedDate.Value, editDepartureDatePicker.SelectedDate.Value, editCustomerNotesTxt.Text);
            MessageBox.Show("Booking Updated.");
            this.Close();
        }
    }
}
