﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HotelManagement
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="NapierHolidays")]
	public partial class HolidayDBDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertNH_Booking(NH_Booking instance);
    partial void UpdateNH_Booking(NH_Booking instance);
    partial void DeleteNH_Booking(NH_Booking instance);
    partial void InsertNH_Guest(NH_Guest instance);
    partial void UpdateNH_Guest(NH_Guest instance);
    partial void DeleteNH_Guest(NH_Guest instance);
    partial void InsertNH_BookingExtra(NH_BookingExtra instance);
    partial void UpdateNH_BookingExtra(NH_BookingExtra instance);
    partial void DeleteNH_BookingExtra(NH_BookingExtra instance);
    partial void InsertNH_BookingGuest(NH_BookingGuest instance);
    partial void UpdateNH_BookingGuest(NH_BookingGuest instance);
    partial void DeleteNH_BookingGuest(NH_BookingGuest instance);
    partial void InsertNH_CarHire(NH_CarHire instance);
    partial void UpdateNH_CarHire(NH_CarHire instance);
    partial void DeleteNH_CarHire(NH_CarHire instance);
    partial void InsertNH_Customer(NH_Customer instance);
    partial void UpdateNH_Customer(NH_Customer instance);
    partial void DeleteNH_Customer(NH_Customer instance);
    partial void InsertNH_Extra(NH_Extra instance);
    partial void UpdateNH_Extra(NH_Extra instance);
    partial void DeleteNH_Extra(NH_Extra instance);
    #endregion
		
		public HolidayDBDataContext() : 
				base(global::HotelManagement.Properties.Settings.Default.NapierHolidaysConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public HolidayDBDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public HolidayDBDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public HolidayDBDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public HolidayDBDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<NH_Booking> NH_Bookings
		{
			get
			{
				return this.GetTable<NH_Booking>();
			}
		}
		
		public System.Data.Linq.Table<NH_Guest> NH_Guests
		{
			get
			{
				return this.GetTable<NH_Guest>();
			}
		}
		
		public System.Data.Linq.Table<NH_BookingExtra> NH_BookingExtras
		{
			get
			{
				return this.GetTable<NH_BookingExtra>();
			}
		}
		
		public System.Data.Linq.Table<NH_BookingGuest> NH_BookingGuests
		{
			get
			{
				return this.GetTable<NH_BookingGuest>();
			}
		}
		
		public System.Data.Linq.Table<NH_CarHire> NH_CarHires
		{
			get
			{
				return this.GetTable<NH_CarHire>();
			}
		}
		
		public System.Data.Linq.Table<NH_Customer> NH_Customers
		{
			get
			{
				return this.GetTable<NH_Customer>();
			}
		}
		
		public System.Data.Linq.Table<NH_Extra> NH_Extras
		{
			get
			{
				return this.GetTable<NH_Extra>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_Booking")]
	public partial class NH_Booking : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ReferenceNum;
		
		private int _CustomerRefNum;
		
		private int _NumOfGuests;
		
		private System.DateTime _ArrivalDate;
		
		private System.DateTime _DepartureDate;
		
		private System.Nullable<decimal> _Payment;
		
		private string _Notes;
		
		private System.Nullable<byte> _Breakfasts;
		
		private System.Nullable<byte> _Dinners;
		
		private System.Nullable<byte> _CarHires;
		
		private EntitySet<NH_BookingExtra> _NH_BookingExtras;
		
		private EntitySet<NH_BookingGuest> _NH_BookingGuests;
		
		private EntityRef<NH_Customer> _NH_Customer;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnReferenceNumChanging(int value);
    partial void OnReferenceNumChanged();
    partial void OnCustomerRefNumChanging(int value);
    partial void OnCustomerRefNumChanged();
    partial void OnNumOfGuestsChanging(int value);
    partial void OnNumOfGuestsChanged();
    partial void OnArrivalDateChanging(System.DateTime value);
    partial void OnArrivalDateChanged();
    partial void OnDepartureDateChanging(System.DateTime value);
    partial void OnDepartureDateChanged();
    partial void OnPaymentChanging(System.Nullable<decimal> value);
    partial void OnPaymentChanged();
    partial void OnNotesChanging(string value);
    partial void OnNotesChanged();
    partial void OnBreakfastsChanging(System.Nullable<byte> value);
    partial void OnBreakfastsChanged();
    partial void OnDinnersChanging(System.Nullable<byte> value);
    partial void OnDinnersChanged();
    partial void OnCarHiresChanging(System.Nullable<byte> value);
    partial void OnCarHiresChanged();
    #endregion
		
		public NH_Booking()
		{
			this._NH_BookingExtras = new EntitySet<NH_BookingExtra>(new Action<NH_BookingExtra>(this.attach_NH_BookingExtras), new Action<NH_BookingExtra>(this.detach_NH_BookingExtras));
			this._NH_BookingGuests = new EntitySet<NH_BookingGuest>(new Action<NH_BookingGuest>(this.attach_NH_BookingGuests), new Action<NH_BookingGuest>(this.detach_NH_BookingGuests));
			this._NH_Customer = default(EntityRef<NH_Customer>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ReferenceNum", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int ReferenceNum
		{
			get
			{
				return this._ReferenceNum;
			}
			set
			{
				if ((this._ReferenceNum != value))
				{
					this.OnReferenceNumChanging(value);
					this.SendPropertyChanging();
					this._ReferenceNum = value;
					this.SendPropertyChanged("ReferenceNum");
					this.OnReferenceNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CustomerRefNum", DbType="Int NOT NULL")]
		public int CustomerRefNum
		{
			get
			{
				return this._CustomerRefNum;
			}
			set
			{
				if ((this._CustomerRefNum != value))
				{
					if (this._NH_Customer.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnCustomerRefNumChanging(value);
					this.SendPropertyChanging();
					this._CustomerRefNum = value;
					this.SendPropertyChanged("CustomerRefNum");
					this.OnCustomerRefNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_NumOfGuests", DbType="Int NOT NULL")]
		public int NumOfGuests
		{
			get
			{
				return this._NumOfGuests;
			}
			set
			{
				if ((this._NumOfGuests != value))
				{
					this.OnNumOfGuestsChanging(value);
					this.SendPropertyChanging();
					this._NumOfGuests = value;
					this.SendPropertyChanged("NumOfGuests");
					this.OnNumOfGuestsChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ArrivalDate", DbType="DateTime NOT NULL")]
		public System.DateTime ArrivalDate
		{
			get
			{
				return this._ArrivalDate;
			}
			set
			{
				if ((this._ArrivalDate != value))
				{
					this.OnArrivalDateChanging(value);
					this.SendPropertyChanging();
					this._ArrivalDate = value;
					this.SendPropertyChanged("ArrivalDate");
					this.OnArrivalDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DepartureDate", DbType="DateTime NOT NULL")]
		public System.DateTime DepartureDate
		{
			get
			{
				return this._DepartureDate;
			}
			set
			{
				if ((this._DepartureDate != value))
				{
					this.OnDepartureDateChanging(value);
					this.SendPropertyChanging();
					this._DepartureDate = value;
					this.SendPropertyChanged("DepartureDate");
					this.OnDepartureDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Payment", DbType="Decimal(10,2)")]
		public System.Nullable<decimal> Payment
		{
			get
			{
				return this._Payment;
			}
			set
			{
				if ((this._Payment != value))
				{
					this.OnPaymentChanging(value);
					this.SendPropertyChanging();
					this._Payment = value;
					this.SendPropertyChanged("Payment");
					this.OnPaymentChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Notes", DbType="Text", UpdateCheck=UpdateCheck.Never)]
		public string Notes
		{
			get
			{
				return this._Notes;
			}
			set
			{
				if ((this._Notes != value))
				{
					this.OnNotesChanging(value);
					this.SendPropertyChanging();
					this._Notes = value;
					this.SendPropertyChanged("Notes");
					this.OnNotesChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Breakfasts", DbType="TinyInt")]
		public System.Nullable<byte> Breakfasts
		{
			get
			{
				return this._Breakfasts;
			}
			set
			{
				if ((this._Breakfasts != value))
				{
					this.OnBreakfastsChanging(value);
					this.SendPropertyChanging();
					this._Breakfasts = value;
					this.SendPropertyChanged("Breakfasts");
					this.OnBreakfastsChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Dinners", DbType="TinyInt")]
		public System.Nullable<byte> Dinners
		{
			get
			{
				return this._Dinners;
			}
			set
			{
				if ((this._Dinners != value))
				{
					this.OnDinnersChanging(value);
					this.SendPropertyChanging();
					this._Dinners = value;
					this.SendPropertyChanged("Dinners");
					this.OnDinnersChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CarHires", DbType="TinyInt")]
		public System.Nullable<byte> CarHires
		{
			get
			{
				return this._CarHires;
			}
			set
			{
				if ((this._CarHires != value))
				{
					this.OnCarHiresChanging(value);
					this.SendPropertyChanging();
					this._CarHires = value;
					this.SendPropertyChanged("CarHires");
					this.OnCarHiresChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_Booking_NH_BookingExtra", Storage="_NH_BookingExtras", ThisKey="ReferenceNum", OtherKey="BookingRefNum")]
		public EntitySet<NH_BookingExtra> NH_BookingExtras
		{
			get
			{
				return this._NH_BookingExtras;
			}
			set
			{
				this._NH_BookingExtras.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_Booking_NH_BookingGuest", Storage="_NH_BookingGuests", ThisKey="ReferenceNum", OtherKey="BookingRefNum")]
		public EntitySet<NH_BookingGuest> NH_BookingGuests
		{
			get
			{
				return this._NH_BookingGuests;
			}
			set
			{
				this._NH_BookingGuests.Assign(value);
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_Customer_NH_Booking", Storage="_NH_Customer", ThisKey="CustomerRefNum", OtherKey="ReferenceNum", IsForeignKey=true)]
		public NH_Customer NH_Customer
		{
			get
			{
				return this._NH_Customer.Entity;
			}
			set
			{
				NH_Customer previousValue = this._NH_Customer.Entity;
				if (((previousValue != value) 
							|| (this._NH_Customer.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._NH_Customer.Entity = null;
						previousValue.NH_Bookings.Remove(this);
					}
					this._NH_Customer.Entity = value;
					if ((value != null))
					{
						value.NH_Bookings.Add(this);
						this._CustomerRefNum = value.ReferenceNum;
					}
					else
					{
						this._CustomerRefNum = default(int);
					}
					this.SendPropertyChanged("NH_Customer");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_NH_BookingExtras(NH_BookingExtra entity)
		{
			this.SendPropertyChanging();
			entity.NH_Booking = this;
		}
		
		private void detach_NH_BookingExtras(NH_BookingExtra entity)
		{
			this.SendPropertyChanging();
			entity.NH_Booking = null;
		}
		
		private void attach_NH_BookingGuests(NH_BookingGuest entity)
		{
			this.SendPropertyChanging();
			entity.NH_Booking = this;
		}
		
		private void detach_NH_BookingGuests(NH_BookingGuest entity)
		{
			this.SendPropertyChanging();
			entity.NH_Booking = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_Guest")]
	public partial class NH_Guest : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private string _PassportNum;
		
		private string _FirstName;
		
		private string _LastName;
		
		private byte _Age;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnPassportNumChanging(string value);
    partial void OnPassportNumChanged();
    partial void OnFirstNameChanging(string value);
    partial void OnFirstNameChanged();
    partial void OnLastNameChanging(string value);
    partial void OnLastNameChanged();
    partial void OnAgeChanging(byte value);
    partial void OnAgeChanged();
    #endregion
		
		public NH_Guest()
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_PassportNum", DbType="NChar(10) NOT NULL", CanBeNull=false, IsPrimaryKey=true)]
		public string PassportNum
		{
			get
			{
				return this._PassportNum;
			}
			set
			{
				if ((this._PassportNum != value))
				{
					this.OnPassportNumChanging(value);
					this.SendPropertyChanging();
					this._PassportNum = value;
					this.SendPropertyChanged("PassportNum");
					this.OnPassportNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FirstName", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string FirstName
		{
			get
			{
				return this._FirstName;
			}
			set
			{
				if ((this._FirstName != value))
				{
					this.OnFirstNameChanging(value);
					this.SendPropertyChanging();
					this._FirstName = value;
					this.SendPropertyChanged("FirstName");
					this.OnFirstNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastName", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string LastName
		{
			get
			{
				return this._LastName;
			}
			set
			{
				if ((this._LastName != value))
				{
					this.OnLastNameChanging(value);
					this.SendPropertyChanging();
					this._LastName = value;
					this.SendPropertyChanged("LastName");
					this.OnLastNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Age", DbType="TinyInt NOT NULL")]
		public byte Age
		{
			get
			{
				return this._Age;
			}
			set
			{
				if ((this._Age != value))
				{
					this.OnAgeChanging(value);
					this.SendPropertyChanging();
					this._Age = value;
					this.SendPropertyChanged("Age");
					this.OnAgeChanged();
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_BookingExtra")]
	public partial class NH_BookingExtra : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private int _BookingRefNum;
		
		private int _ExtraId;
		
		private EntityRef<NH_Booking> _NH_Booking;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnBookingRefNumChanging(int value);
    partial void OnBookingRefNumChanged();
    partial void OnExtraIdChanging(int value);
    partial void OnExtraIdChanged();
    #endregion
		
		public NH_BookingExtra()
		{
			this._NH_Booking = default(EntityRef<NH_Booking>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_BookingRefNum", DbType="Int NOT NULL")]
		public int BookingRefNum
		{
			get
			{
				return this._BookingRefNum;
			}
			set
			{
				if ((this._BookingRefNum != value))
				{
					if (this._NH_Booking.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnBookingRefNumChanging(value);
					this.SendPropertyChanging();
					this._BookingRefNum = value;
					this.SendPropertyChanged("BookingRefNum");
					this.OnBookingRefNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExtraId", DbType="Int NOT NULL")]
		public int ExtraId
		{
			get
			{
				return this._ExtraId;
			}
			set
			{
				if ((this._ExtraId != value))
				{
					this.OnExtraIdChanging(value);
					this.SendPropertyChanging();
					this._ExtraId = value;
					this.SendPropertyChanged("ExtraId");
					this.OnExtraIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_Booking_NH_BookingExtra", Storage="_NH_Booking", ThisKey="BookingRefNum", OtherKey="ReferenceNum", IsForeignKey=true)]
		public NH_Booking NH_Booking
		{
			get
			{
				return this._NH_Booking.Entity;
			}
			set
			{
				NH_Booking previousValue = this._NH_Booking.Entity;
				if (((previousValue != value) 
							|| (this._NH_Booking.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._NH_Booking.Entity = null;
						previousValue.NH_BookingExtras.Remove(this);
					}
					this._NH_Booking.Entity = value;
					if ((value != null))
					{
						value.NH_BookingExtras.Add(this);
						this._BookingRefNum = value.ReferenceNum;
					}
					else
					{
						this._BookingRefNum = default(int);
					}
					this.SendPropertyChanged("NH_Booking");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_BookingGuest")]
	public partial class NH_BookingGuest : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private int _BookingRefNum;
		
		private string _GuestPassNum;
		
		private EntityRef<NH_Booking> _NH_Booking;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnBookingRefNumChanging(int value);
    partial void OnBookingRefNumChanged();
    partial void OnGuestPassNumChanging(string value);
    partial void OnGuestPassNumChanged();
    #endregion
		
		public NH_BookingGuest()
		{
			this._NH_Booking = default(EntityRef<NH_Booking>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_BookingRefNum", DbType="Int NOT NULL")]
		public int BookingRefNum
		{
			get
			{
				return this._BookingRefNum;
			}
			set
			{
				if ((this._BookingRefNum != value))
				{
					if (this._NH_Booking.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnBookingRefNumChanging(value);
					this.SendPropertyChanging();
					this._BookingRefNum = value;
					this.SendPropertyChanged("BookingRefNum");
					this.OnBookingRefNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_GuestPassNum", DbType="NChar(10) NOT NULL", CanBeNull=false)]
		public string GuestPassNum
		{
			get
			{
				return this._GuestPassNum;
			}
			set
			{
				if ((this._GuestPassNum != value))
				{
					this.OnGuestPassNumChanging(value);
					this.SendPropertyChanging();
					this._GuestPassNum = value;
					this.SendPropertyChanged("GuestPassNum");
					this.OnGuestPassNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_Booking_NH_BookingGuest", Storage="_NH_Booking", ThisKey="BookingRefNum", OtherKey="ReferenceNum", IsForeignKey=true)]
		public NH_Booking NH_Booking
		{
			get
			{
				return this._NH_Booking.Entity;
			}
			set
			{
				NH_Booking previousValue = this._NH_Booking.Entity;
				if (((previousValue != value) 
							|| (this._NH_Booking.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._NH_Booking.Entity = null;
						previousValue.NH_BookingGuests.Remove(this);
					}
					this._NH_Booking.Entity = value;
					if ((value != null))
					{
						value.NH_BookingGuests.Add(this);
						this._BookingRefNum = value.ReferenceNum;
					}
					else
					{
						this._BookingRefNum = default(int);
					}
					this.SendPropertyChanged("NH_Booking");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_CarHire")]
	public partial class NH_CarHire : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _Id;
		
		private string _Driver;
		
		private System.DateTime _StartDate;
		
		private System.DateTime _EndDate;
		
		private EntitySet<NH_Extra> _NH_Extras;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdChanging(int value);
    partial void OnIdChanged();
    partial void OnDriverChanging(string value);
    partial void OnDriverChanged();
    partial void OnStartDateChanging(System.DateTime value);
    partial void OnStartDateChanged();
    partial void OnEndDateChanging(System.DateTime value);
    partial void OnEndDateChanged();
    #endregion
		
		public NH_CarHire()
		{
			this._NH_Extras = new EntitySet<NH_Extra>(new Action<NH_Extra>(this.attach_NH_Extras), new Action<NH_Extra>(this.detach_NH_Extras));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Id", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int Id
		{
			get
			{
				return this._Id;
			}
			set
			{
				if ((this._Id != value))
				{
					this.OnIdChanging(value);
					this.SendPropertyChanging();
					this._Id = value;
					this.SendPropertyChanged("Id");
					this.OnIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Driver", DbType="NVarChar(100) NOT NULL", CanBeNull=false)]
		public string Driver
		{
			get
			{
				return this._Driver;
			}
			set
			{
				if ((this._Driver != value))
				{
					this.OnDriverChanging(value);
					this.SendPropertyChanging();
					this._Driver = value;
					this.SendPropertyChanged("Driver");
					this.OnDriverChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_StartDate", DbType="DateTime NOT NULL")]
		public System.DateTime StartDate
		{
			get
			{
				return this._StartDate;
			}
			set
			{
				if ((this._StartDate != value))
				{
					this.OnStartDateChanging(value);
					this.SendPropertyChanging();
					this._StartDate = value;
					this.SendPropertyChanged("StartDate");
					this.OnStartDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_EndDate", DbType="DateTime NOT NULL")]
		public System.DateTime EndDate
		{
			get
			{
				return this._EndDate;
			}
			set
			{
				if ((this._EndDate != value))
				{
					this.OnEndDateChanging(value);
					this.SendPropertyChanging();
					this._EndDate = value;
					this.SendPropertyChanged("EndDate");
					this.OnEndDateChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_CarHire_NH_Extra", Storage="_NH_Extras", ThisKey="Id", OtherKey="CarHireId")]
		public EntitySet<NH_Extra> NH_Extras
		{
			get
			{
				return this._NH_Extras;
			}
			set
			{
				this._NH_Extras.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_NH_Extras(NH_Extra entity)
		{
			this.SendPropertyChanging();
			entity.NH_CarHire = this;
		}
		
		private void detach_NH_Extras(NH_Extra entity)
		{
			this.SendPropertyChanging();
			entity.NH_CarHire = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_Customer")]
	public partial class NH_Customer : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ReferenceNum;
		
		private string _FirstName;
		
		private string _LastName;
		
		private string _Address;
		
		private EntitySet<NH_Booking> _NH_Bookings;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnReferenceNumChanging(int value);
    partial void OnReferenceNumChanged();
    partial void OnFirstNameChanging(string value);
    partial void OnFirstNameChanged();
    partial void OnLastNameChanging(string value);
    partial void OnLastNameChanged();
    partial void OnAddressChanging(string value);
    partial void OnAddressChanged();
    #endregion
		
		public NH_Customer()
		{
			this._NH_Bookings = new EntitySet<NH_Booking>(new Action<NH_Booking>(this.attach_NH_Bookings), new Action<NH_Booking>(this.detach_NH_Bookings));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ReferenceNum", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int ReferenceNum
		{
			get
			{
				return this._ReferenceNum;
			}
			set
			{
				if ((this._ReferenceNum != value))
				{
					this.OnReferenceNumChanging(value);
					this.SendPropertyChanging();
					this._ReferenceNum = value;
					this.SendPropertyChanged("ReferenceNum");
					this.OnReferenceNumChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_FirstName", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string FirstName
		{
			get
			{
				return this._FirstName;
			}
			set
			{
				if ((this._FirstName != value))
				{
					this.OnFirstNameChanging(value);
					this.SendPropertyChanging();
					this._FirstName = value;
					this.SendPropertyChanged("FirstName");
					this.OnFirstNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_LastName", DbType="NVarChar(64) NOT NULL", CanBeNull=false)]
		public string LastName
		{
			get
			{
				return this._LastName;
			}
			set
			{
				if ((this._LastName != value))
				{
					this.OnLastNameChanging(value);
					this.SendPropertyChanging();
					this._LastName = value;
					this.SendPropertyChanged("LastName");
					this.OnLastNameChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Address", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string Address
		{
			get
			{
				return this._Address;
			}
			set
			{
				if ((this._Address != value))
				{
					this.OnAddressChanging(value);
					this.SendPropertyChanging();
					this._Address = value;
					this.SendPropertyChanged("Address");
					this.OnAddressChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_Customer_NH_Booking", Storage="_NH_Bookings", ThisKey="ReferenceNum", OtherKey="CustomerRefNum")]
		public EntitySet<NH_Booking> NH_Bookings
		{
			get
			{
				return this._NH_Bookings;
			}
			set
			{
				this._NH_Bookings.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_NH_Bookings(NH_Booking entity)
		{
			this.SendPropertyChanging();
			entity.NH_Customer = this;
		}
		
		private void detach_NH_Bookings(NH_Booking entity)
		{
			this.SendPropertyChanging();
			entity.NH_Customer = null;
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.NH_Extra")]
	public partial class NH_Extra : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _ExtraId;
		
		private System.Nullable<int> _CarHireId;
		
		private string _Description;
		
		private decimal _Cost;
		
		private EntityRef<NH_CarHire> _NH_CarHire;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnExtraIdChanging(int value);
    partial void OnExtraIdChanged();
    partial void OnCarHireIdChanging(System.Nullable<int> value);
    partial void OnCarHireIdChanged();
    partial void OnDescriptionChanging(string value);
    partial void OnDescriptionChanged();
    partial void OnCostChanging(decimal value);
    partial void OnCostChanged();
    #endregion
		
		public NH_Extra()
		{
			this._NH_CarHire = default(EntityRef<NH_CarHire>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_ExtraId", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int ExtraId
		{
			get
			{
				return this._ExtraId;
			}
			set
			{
				if ((this._ExtraId != value))
				{
					this.OnExtraIdChanging(value);
					this.SendPropertyChanging();
					this._ExtraId = value;
					this.SendPropertyChanged("ExtraId");
					this.OnExtraIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_CarHireId", DbType="Int")]
		public System.Nullable<int> CarHireId
		{
			get
			{
				return this._CarHireId;
			}
			set
			{
				if ((this._CarHireId != value))
				{
					if (this._NH_CarHire.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnCarHireIdChanging(value);
					this.SendPropertyChanging();
					this._CarHireId = value;
					this.SendPropertyChanged("CarHireId");
					this.OnCarHireIdChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Description", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string Description
		{
			get
			{
				return this._Description;
			}
			set
			{
				if ((this._Description != value))
				{
					this.OnDescriptionChanging(value);
					this.SendPropertyChanging();
					this._Description = value;
					this.SendPropertyChanged("Description");
					this.OnDescriptionChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Cost", DbType="Decimal(10,2) NOT NULL")]
		public decimal Cost
		{
			get
			{
				return this._Cost;
			}
			set
			{
				if ((this._Cost != value))
				{
					this.OnCostChanging(value);
					this.SendPropertyChanging();
					this._Cost = value;
					this.SendPropertyChanged("Cost");
					this.OnCostChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="NH_CarHire_NH_Extra", Storage="_NH_CarHire", ThisKey="CarHireId", OtherKey="Id", IsForeignKey=true)]
		public NH_CarHire NH_CarHire
		{
			get
			{
				return this._NH_CarHire.Entity;
			}
			set
			{
				NH_CarHire previousValue = this._NH_CarHire.Entity;
				if (((previousValue != value) 
							|| (this._NH_CarHire.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._NH_CarHire.Entity = null;
						previousValue.NH_Extras.Remove(this);
					}
					this._NH_CarHire.Entity = value;
					if ((value != null))
					{
						value.NH_Extras.Add(this);
						this._CarHireId = value.Id;
					}
					else
					{
						this._CarHireId = default(Nullable<int>);
					}
					this.SendPropertyChanged("NH_CarHire");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
#pragma warning restore 1591
