﻿namespace HotelManagement
{
    interface ExtraItem
    {
        string Description { get; set; }
        decimal Cost { get; set; }
        void AddNewExtra();
    }
}